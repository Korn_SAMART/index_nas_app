const express = require("express");
const cors = require("cors");
const mongoose = require('mongoose');

var app = express();
app.use(cors());

// คำสั่งเชื่อม MongoDB Atlas
var mongo_uri = 'mongodb://localhost:27017/dol'
mongoose.Promise = global.Promise;

mongoose
  .connect(mongo_uri, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(
    () => {
      console.log("[success] task: connected to the database");
    },
    (error) => {
      console.log("[failed] task: " + error);
      process.exit(1);
    }
  );

app.get('/', (req, res) => {
  res.send("Welcome")
})

const PORT = process.env.PORT || 9998
app.listen(PORT, () => {
  console.log(`🚀 Server ready at http://localhost:${PORT}`);
});

//Router
var Dropdown = require("./routes/dropdown");
var Search = require("./routes/search");
var Dashboard = require("./routes/dashboard");
app.use("/api/dropdown", Dropdown);
app.use("/api/search", Search);
app.use("/api/dashboard", Dashboard);

app.use((req, res, next) => {
  var err = new Error("ไม่พบ path ที่คุณต้องการ");
  err.status = 404;
  next(err);
});