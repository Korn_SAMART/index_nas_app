const mongoose = require('mongoose');

const Mappings = mongoose.Schema({
  REG_COUNT: {
    type: String
  },
  IMG_COUNT: {
    type: String
  },
  MAPDOL_COUNT: {
    type: String
  },
  INDEX_COUNT: {
    type: String
  }
})

module.exports = mongoose.model('summary_cards', Mappings);