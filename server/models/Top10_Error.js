const mongoose = require('mongoose');

const Top10 = mongoose.Schema({
  id: { type: String },
  TYPE: { type: String },
  LANDOFFICE_NAME_TH: { type: String },
  LANDOFFICE_SEQ: { type: String },
  COUNT: { type: Number },
})

module.exports = mongoose.model('top10_errors', Top10);