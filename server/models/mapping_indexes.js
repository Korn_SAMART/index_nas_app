const mongoose = require('mongoose');

const Mappings = mongoose.Schema({
  id: {
    type: String
  },
  LANDOFFICE_SEQ: {
    type: String
  },
  PRINTPLATE_TYPE_SEQ: {
    type: String
  },
  PROVINCE_SEQ: {
    type: String
  },
  AMPHUR_SEQ: {
    type: String
  },
  PARCEL_NO: {
    type: String
  },
  PARCEL_SURVEY_NO: {
    type: String
  },
  OWNER_NAME: {
    type: String
  },
  PARCEL_UTMMAP1: {
    type: String
  },
  PARCEL_UTMMAP2: {
    type: String
  },
  PARCEL_UTMMAP3: {
    type: String
  },
  PARCEL_UTMMAP4: {
    type: String
  },
  UTMSCALE_SEQ: {
    type: String
  },
  PARCEL_UTM_LAND_NO: {
    type: String
  },
  STS_UPD: {
    type: String
  }
})

module.exports = mongoose.model('mapping_indexes', Mappings);