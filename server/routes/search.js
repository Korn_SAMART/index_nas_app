var express = require("express");
var oracledb = require("oracledb");
var router = express.Router();

oracledb.outFormat = oracledb.OBJECT;
router.get("/", async (req, res) => {
  let connection = await oracledb.getConnection({ user: "reg", password: "doldol", connectionString: "203.146.170.169:1521/DOL2" });
  let amphur_condition = '', tambol_condition = '';
  try {
    if (req.query.amphur_seq) amphur_condition = ' AND P.AMPHUR_SEQ = ' + req.query.amphur_seq
    if (req.query.tambol_seq) tambol_condition = ' AND P.TAMBOL_SEQ = ' + req.query.tambol_seq

    let query = '';
    if (req.query.printplate_type_seq == 0) {
      query = "WITH P2 AS ( "
        + "SELECT P.PARCEL_SEQ,P.PARCEL_NO, AP.AMPHUR_NAME, TB.TAMBOL_NAME, P.PARCEL_SURVEY_NO, I.PARCEL_IMAGE_ORDER, "
        + "I.PARCEL_IMAGE_PNO, "
        + "I.PARCEL_IMAGE_PNAME, "
        + "CONCAT(I.PARCEL_IMAGE_URL,CONCAT('/', I.PARCEL_IMAGE_FILENAME)) AS PARCEL_IMAGE_PATH, "
        + "I.PARCEL_IMAGE_FILENAME FROM REG.TB_REG_PARCEL_IMAGE I "
        + "INNER JOIN REG.TB_REG_PARCEL P "
        + "ON I.PARCEL_SEQ = P.PARCEL_SEQ "
        + "LEFT JOIN MAS.TB_MAS_AMPHUR AP "
        + "ON AP.AMPHUR_SEQ = P.AMPHUR_SEQ "
        + "LEFT JOIN MAS.TB_MAS_TAMBOL TB "
        + "ON TB.TAMBOL_SEQ = P.TAMBOL_SEQ "
        + "WHERE TO_NUMBER(regexp_replace(P.PARCEL_NO, '[^0-9]', '')) between " + req.query.start_seq + " and " + req.query.end_seq + " AND P.LANDOFFICE_SEQ = " + req.query.landoffice_seq + amphur_condition + tambol_condition
        + " ORDER BY P.PARCEL_NO "
        + "), "
        + "P0 AS ( "
        + "  SELECT P.PARCEL_SEQ, P.PARCEL_NO, AP.AMPHUR_NAME, TB.TAMBOL_NAME, P.PARCEL_SURVEY_NO, I.PARCEL_IMAGE_ORDER,  "
        + "  I.PARCEL_IMAGE_PNO, "
        + "   I.PARCEL_IMAGE_PNAME, "
        + "   CONCAT(I.PARCEL_IMAGE_URL,CONCAT('/', I.PARCEL_IMAGE_FILENAME)) AS PARCEL_IMAGE_PATH, "
        + "    I.PARCEL_IMAGE_FILENAME FROM MGT0.TB_REG_PARCEL_IMAGE I "
        + "    INNER JOIN MGT0.TB_REG_PARCEL P "
        + "     ON I.PARCEL_SEQ = P.PARCEL_SEQ "
        + "     LEFT JOIN MAS.TB_MAS_AMPHUR AP "
        + "     ON AP.AMPHUR_SEQ = P.AMPHUR_SEQ "
        + "      LEFT JOIN MAS.TB_MAS_TAMBOL TB "
        + "      ON TB.TAMBOL_SEQ = P.TAMBOL_SEQ "
        + "      WHERE TO_NUMBER(regexp_replace(P.PARCEL_NO, '[^0-9]', '')) between " + req.query.start_seq + " and " + req.query.end_seq + " AND P.LANDOFFICE_SEQ = " + req.query.landoffice_seq + amphur_condition + tambol_condition
        + "       ORDER BY P.PARCEL_NO "
        + "     ) "
        + "     SELECT (ROW_NUMBER() OVER(PARTITION BY 100 ORDER BY 1)) AS \"id\", P2.PARCEL_SEQ, CONCAT(P0.PARCEL_SEQ, CONCAT(',', P2.PARCEL_SEQ)) AS PARCEL_SEQ_TEMP, P2.PARCEL_NO, P2.AMPHUR_NAME, P2.TAMBOL_NAME, P2.PARCEL_SURVEY_NO, P2.PARCEL_IMAGE_ORDER,  "
        + "    P2.PARCEL_IMAGE_PNO, "
        + "    P2.PARCEL_IMAGE_PNAME, "
        + "    P2.PARCEL_IMAGE_FILENAME AS PARCEL_IMAGE_FILENAME_P2, "
        + "    P0.PARCEL_IMAGE_FILENAME AS PARCEL_IMAGE_FILENAME_P0, "
        + "    CONCAT(P0.PARCEL_IMAGE_PATH, CONCAT(',', P2.PARCEL_IMAGE_PATH)) AS PATH FROM P2"
        + "    FULL OUTER JOIN P0 "
        + "    ON P0.PARCEL_SEQ = P2.PARCEL_SEQ";
    }
    else {
      query = "WITH P2 AS ( "
        + "    SELECT P.PARCEL_lAND_SEQ,P.PARCEL_LAND_NO, AP.AMPHUR_NAME, TB.TAMBOL_NAME, P.PARCEL_LAND_SURVEY_NO, I.PARCEL_LAND_IMAGE_ORDER, "
        + "    I.PARCEL_LAND_IMAGE_PNO, "
        + "    CONCAT(I.PARCEL_LAND_IMAGE_URL,CONCAT('/', I.PARCEL_LAND_IMAGE_FILENAME)) AS PARCEL_IMAGE_PATH, P.PRINTPLATE_TYPE_SEQ, "
        + "    I.PARCEL_LAND_IMAGE_PNAME FROM REG.TB_REG_PARCEL_LAND_IMAGE I "
        + "    INNER JOIN REG.TB_REG_PARCEL_LAND P "
        + "    ON I.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ "
        + "    LEFT JOIN MAS.TB_MAS_AMPHUR AP "
        + "    ON AP.AMPHUR_SEQ = P.AMPHUR_SEQ "
        + "    LEFT JOIN MAS.TB_MAS_TAMBOL TB "
        + "    ON TB.TAMBOL_SEQ = P.TAMBOL_SEQ "
        + "    WHERE TO_NUMBER(regexp_replace(P.PARCEL_LAND_NO, '[^0-9]', '')) between " + req.query.start_seq + " and " + req.query.end_seq + " AND P.PRINTPLATE_TYPE_SEQ = " + req.query.printplate_type_seq + " AND P.LANDOFFICE_SEQ = " + req.query.landoffice_seq + amphur_condition + tambol_condition
        + "     ORDER BY P.PARCEL_LAND_NO "
        + "    ), "
        + "    P0 AS ( "
        + "    SELECT P.PARCEL_lAND_SEQ,P.PARCEL_LAND_NO, AP.AMPHUR_NAME, TB.TAMBOL_NAME, P.PARCEL_LAND_SURVEY_NO, I.PARCEL_LAND_IMAGE_ORDER, "
        + "    I.PARCEL_LAND_IMAGE_PNO, "
        + "    CONCAT(I.PARCEL_LAND_IMAGE_URL,CONCAT('/', I.PARCEL_LAND_IMAGE_FILENAME)) AS PARCEL_IMAGE_PATH, P.PRINTPLATE_TYPE_SEQ, "
        + "    I.PARCEL_LAND_IMAGE_PNAME FROM MGT0.TB_REG_PARCEL_LAND_IMAGE I "
        + "        INNER JOIN MGT0.TB_REG_PARCEL_LAND P "
        + "      ON I.PARCEL_LAND_SEQ = P.PARCEL_LAND_SEQ "
        + "         LEFT JOIN MAS.TB_MAS_AMPHUR AP "
        + "         ON AP.AMPHUR_SEQ = P.AMPHUR_SEQ "
        + "          LEFT JOIN MAS.TB_MAS_TAMBOL TB "
        + "          ON TB.TAMBOL_SEQ = P.TAMBOL_SEQ "
        + "    WHERE TO_NUMBER(regexp_replace(P.PARCEL_LAND_NO, '[^0-9]', '')) between " + req.query.start_seq + " and " + req.query.end_seq + " AND P.PRINTPLATE_TYPE_SEQ = " + req.query.printplate_type_seq + " AND P.LANDOFFICE_SEQ = " + req.query.landoffice_seq + amphur_condition + tambol_condition
        + "     ORDER BY P.PARCEL_LAND_NO "
        + "         ) "
        + "         SELECT (ROW_NUMBER() OVER(PARTITION BY 100 ORDER BY 1)) AS \"id\", P2.PARCEL_LAND_SEQ, CONCAT(P0.PARCEL_LAND_SEQ, CONCAT(',', P2.PARCEL_LAND_SEQ)) AS PARCEL_SEQ_TEMP, P2.PARCEL_LAND_NO AS PARCEL_NO, P2.AMPHUR_NAME, P2.TAMBOL_NAME, P2.PARCEL_LAND_SURVEY_NO AS PARCEL_SURVEY_NO, P2.PARCEL_LAND_IMAGE_ORDER AS PARCEL_IMAGE_ORDER, "
        + "        P2.PARCEL_LAND_IMAGE_PNO AS PARCEL_IMAGE_PNO, "
        + "        P2.PARCEL_LAND_IMAGE_PNAME, "
        + "        P2.PARCEL_IMAGE_PATH AS PARCEL_IMAGE_FILENAME_P2, "
        + "        P0.PARCEL_IMAGE_PATH AS PARCEL_IMAGE_FILENAME_P0, "
        + "        CONCAT(P0.PARCEL_IMAGE_PATH, CONCAT(',', P2.PARCEL_IMAGE_PATH)) AS PATH FROM P2 "
        + "        FULL OUTER JOIN P0 "
        + "    ON P0.PARCEL_LAND_SEQ = P2.PARCEL_LAND_SEQ";
    }

    console.log(query);


    let result = await connection.execute(query).then(data => {
      return data.rows;
    });

    res.json(result);

  } catch (err) {
    res.send(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }

});

router.get('/hsfs', async (req, res) => {
  let connection = await oracledb.getConnection({ user: "reg", password: "doldol", connectionString: "203.146.170.169:1521/DOL2" });
  let table = '';
  let table_type = req.query.search_type == 1 ? 'MGT1' : 'MGT0';
  if (req.query.printplate_type == 0) table = 'PARCEL';
  else if (req.query.printplate_type == 10) table = 'CONDOROOM';
  else if (req.query.printplate_type == 13) table = 'CONDO';
  else table = 'PARCEL_LAND';

  console.log("table = ", table);


  try {
    let query = "WITH RECV AS ( "
      + "SELECT DISTINCT " + table + "_INDEX_REGDTM, " + table + "_INDEX_ORDER AS " + table + "_INDEX_ORDER, REGISTER_SEQ, " + table + "_INDEX_REGNAME, " + table + "_HSFS_ORDER, " + table + "_HSFS_PNO, COUNT(DOCUMENT_SEQ) OVER (PARTITION BY PH." + table + "_INDEX_SEQ, DOCUMENT_SEQ) AS NUM"
      + " , DOCUMENT_SEQ, " + table + "_HSFS_PNAME, " + table + "_HSFS_URL, " + table + "_HSFS_FILENAME, " + table + "_HSFS_SEQ,"
      + " CONCAT(" + table + "_HSFS_URL,CONCAT('/', " + table + "_HSFS_FILENAME)) AS HSFS_PATH"
      + " FROM " + table_type + ".TB_REG_" + table + "_HSFS PH"
      + " INNER JOIN " + table_type + ".TB_REG_" + table + "_INDEX PI"
      + " ON PI." + table + "_INDEX_SEQ = PH." + table + "_INDEX_SEQ"
      + " AND PI.RECORD_STATUS = 'N'"
      + " WHERE PH.RECORD_STATUS IN ('N', 'W', 'E') ";
    if (req.query.parcelseq1) query += "AND " + table + "_SEQ = " + req.query.parcelseq1;

    query += "), OK AS ( "
      + " SELECT DISTINCT " + table + "_INDEX_REGDTM, " + table + "_INDEX_ORDER AS " + table + "_INDEX_ORDER, REGISTER_SEQ, " + table + "_INDEX_REGNAME, " + table + "_HSFS_ORDER, " + table + "_HSFS_PNO, COUNT(DOCUMENT_SEQ) OVER (PARTITION BY PH." + table + "_INDEX_SEQ, DOCUMENT_SEQ) AS NUM"
      + " , DOCUMENT_SEQ, " + table + "_HSFS_PNAME, " + table + "_HSFS_URL, " + table + "_HSFS_FILENAME, " + table + "_HSFS_SEQ,"
      + " CONCAT(" + table + "_HSFS_URL,CONCAT('/', " + table + "_HSFS_FILENAME)) AS HSFS_PATH"
      + " FROM REG.TB_REG_" + table + "_HSFS PH"
      + " INNER JOIN REG.TB_REG_" + table + "_INDEX PI"
      + " ON PI." + table + "_INDEX_SEQ = PH." + table + "_INDEX_SEQ"
      + " AND PI.RECORD_STATUS = 'N' "
      + " WHERE PH.RECORD_STATUS IN ('N', 'W', 'E') AND " + table + "_SEQ = " + req.query.parcelseq2
      + ") "
      + " SELECT DISTINCT (ROW_NUMBER() OVER(PARTITION BY 100 ORDER BY 1)) AS \"id\", NVL(TO_DATE(RECV." + table + "_INDEX_REGDTM,'DD/MM/YYYY'),TO_DATE(OK." + table + "_INDEX_REGDTM,'DD/MM/YYYY')) AS DTM "
      + " , NVL(RECV." + table + "_INDEX_ORDER,OK." + table + "_INDEX_ORDER) AS ORD_INX, NVL(RECV." + table + "_INDEX_REGNAME,OK." + table + "_INDEX_REGNAME) AS REGIST "
      + " , NVL(RECV." + table + "_HSFS_ORDER,OK." + table + "_HSFS_ORDER) AS ORD_HF, NVL(RECV." + table + "_HSFS_PNO,OK." + table + "_HSFS_PNO) AS CPAGE, NVL(RECV.NUM,OK.NUM) AS NPAGE "
      + " , NVL(RECV." + table + "_HSFS_PNAME,OK." + table + "_HSFS_PNAME) AS DOC_NAME "
      + " , RECV." + table + "_HSFS_URL AS URL, RECV." + table + "_HSFS_FILENAME AS FILENAME, OK." + table + "_HSFS_URL AS URL_2, OK." + table + "_HSFS_FILENAME AS FILENAME_2, "
      + " CONCAT(RECV.HSFS_PATH, CONCAT(',', OK.HSFS_PATH)) AS PATH"
      + " FROM RECV "
      + " RIGHT JOIN OK "
      + " ON RECV." + table + "_HSFS_SEQ = OK." + table + "_HSFS_SEQ"
      + " ORDER BY NVL(TO_DATE(RECV." + table + "_INDEX_REGDTM,'DD/MM/YYYY'),TO_DATE(OK." + table + "_INDEX_REGDTM,'DD/MM/YYYY')), NVL(RECV." + table + "_INDEX_ORDER,OK." + table + "_INDEX_ORDER) "
      + " ,NVL(RECV." + table + "_HSFS_ORDER,OK." + table + "_HSFS_ORDER) ,NVL(RECV." + table + "_HSFS_PNO,OK." + table + "_HSFS_PNO)";

    console.log(query);
    let result = await connection.execute(query).then(data => {
      return data.rows;
    });

    res.json(result);

  } catch (err) {
    res.send(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }
})

router.get("/image_list/:parcel_seq", async (req, res) => {
  let connection = await oracledb.getConnection({ user: "reg", password: "doldol", connectionString: "203.146.170.169:1521/DOL2" });
  try {

    let query = '';
    if (req.query.printplate_type_seq == 0) {
      query = "SELECT PARCEL_IMAGE_PNAME, PARCEL_IMAGE_URL, PARCEL_IMAGE_FILENAME FROM REG.TB_REG_PARCEL_IMAGE WHERE PARCEL_SEQ = " + req.params.parcel_seq;
    }
    else {
      query = "SELECT PARCEL_LAND_IMAGE_PNAME as PARCEL_IMAGE_PNAME, PARCEL_LAND_IMAGE_URL as PARCEL_IMAGE_URL, PARCEL_LAND_IMAGE_FILENAME as PARCEL_IMAGE_FILENAME FROM REG.TB_REG_PARCEL_LAND_IMAGE WHERE PARCEL_LAND_SEQ = " + req.params.parcel_seq;
    }

    console.log(query);

    let result = await connection.execute(query).then(data => {
      return data.rows;
    });

    res.json(result);

  } catch (err) {
    res.send(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }

});

router.get("/hsfs_list/:parcel_seq", async (req, res) => {
  let connection = await oracledb.getConnection({ user: "reg", password: "doldol", connectionString: "203.146.170.169:1521/DOL2" });
  try {

    let query = '';
    if (req.query.printplate_type_seq == 0) {
      query = "SELECT PARCEL_HSFS_PNAME as PARCEL_IMAGE_PNAME, PARCEL_HSFS_URL as PARCEL_IMAGE_URL, PARCEL_HSFS_FILENAME as PARCEL_IMAGE_FILENAME"
        + " FROM REG.TB_REG_PARCEL_INDEX IDX"
        + " INNER JOIN REG.TB_REG_PARCEL_HSFS HSFS"
        + " ON IDX.PARCEL_INDEX_SEQ = HSFS.PARCEL_INDEX_SEQ"
        + " WHERE IDX.PARCEL_SEQ = " + req.params.parcel_seq
    }
    else {
      query = "SELECT PARCEL_LAND_HSFS_PNAME AS PARCEL_IMAGE_PNAME, PARCEL_LAND_HSFS_URL AS PARCEL_IMAGE_URL, PARCEL_LAND_HSFS_FILENAME AS PARCEL_IMAGE_FILENAME"
        + " FROM REG.TB_REG_PARCEL_LAND_INDEX IDX"
        + " INNER JOIN REG.TB_REG_PARCEL_LAND_HSFS HSFS"
        + " ON IDX.PARCEL_LAND_INDEX_SEQ = HSFS.PARCEL_LAND_INDEX_SEQ"
        + " WHERE IDX.PARCEL_LAND_SEQ = " + req.params.parcel_seq
    }



    console.log(query);

    let result = await connection.execute(query).then(data => {
      return data.rows;
    });

    res.json(result);

  } catch (err) {
    res.send(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }

});

module.exports = router;