var express = require("express");
var oracledb = require('oracledb');
var router = express.Router();

oracledb.outFormat = oracledb.OBJECT;
router.get("/", async (req, res) => {
  let connection = await oracledb.getConnection({ user: "reg", password: "doldol", connectionString: "203.146.170.169:1521/DOL2" });

  try {
    let result = await connection.execute("SELECT * from AREA FETCH NEXT 5 ROWS ONLY").then(data => {
      return data.rows;
    });

    res.json(result);

  } catch (err) {
    res.send(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }

});

router.get("/region", async (req, res) => {
  let connection = await oracledb.getConnection({ user: "reg", password: "doldol", connectionString: "203.146.170.169:1521/DOL2" });

  try {

    let result = await connection.execute("SELECT REGION_SEQ, REGION_ID, REGION_NAME FROM MAS.TB_MAS_REGION").then(data => {
        return data.rows;
      });

    res.send(result);

  } catch (err) {
    res.send(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }

});


router.get("/province", async (req, res) => {
  let connection = await oracledb.getConnection({ user: "reg", password: "doldol", connectionString: "203.146.170.169:1521/DOL2" });

  try {

    let result = await connection.execute("SELECT DISTINCT PV.PROVINCE_SEQ, PV.PROVINCE_NAME"
      + " FROM MAS.TB_MAS_PROVINCE PV"
      + " INNER JOIN MAS.TB_MAS_LANDOFFICE L"
      + "     ON L.PROVINCE_SEQ = PV.PROVINCE_SEQ"
      + "     AND L.LANDOFFICE_SEQ IN ("
      + "         SELECT LANDOFFICE_SEQ FROM MAS.TB_MAS_LANDOFFICE"
      + "         WHERE RECORD_STATUS='N'"
      + "         )"
      + " ORDER BY  NLSSORT(PV.PROVINCE_NAME, 'NLS_SORT=THAI_DICTIONARY')").then(data => {
        return data.rows;
      });

    res.send(result);

  } catch (err) {
    res.send(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }

});


router.get("/landoffice/:id", async (req, res) => {
  let connection = await oracledb.getConnection({ user: "reg", password: "doldol", connectionString: "203.146.170.169:1521/DOL2" });

  try {

    let result = await connection.execute("SELECT LANDOFFICE_SEQ, LANDOFFICE_NAME_TH"
      + " FROM MAS.TB_MAS_LANDOFFICE"
      + " WHERE PROVINCE_SEQ = " + req.params.id + " AND (LANDOFFICE_TYPE_SEQ = 6 OR LANDOFFICE_TYPE_SEQ = 7) AND RECORD_STATUS='N'"
      + " ORDER BY NLSSORT(LANDOFFICE_NAME_TH, 'NLS_SORT=THAI_DICTIONARY')").then(data => {
        return data.rows;
      });

    res.send(result);

  } catch (err) {
    res.send(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }

});

router.get("/amphur/:id", async (req, res) => {
  let connection = await oracledb.getConnection({ user: "reg", password: "doldol", connectionString: "203.146.170.169:1521/DOL2" });

  try {
    let result = await connection.execute("SELECT AMPHUR_SEQ, AMPHUR_NAME FROM MAS.TB_MAS_AMPHUR"
      + " WHERE PROVINCE_SEQ = " + req.params.id).then(data => {
        return data.rows;
      });

    res.send(result);

  } catch (err) {
    res.send(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }

});

router.get("/tambon/:id", async (req, res) => {
  let connection = await oracledb.getConnection({ user: "reg", password: "doldol", connectionString: "203.146.170.169:1521/DOL2" });

  try {

    let result = await connection.execute("SELECT TAMBOL_SEQ, TAMBOL_NAME FROM MAS.TB_MAS_TAMBOL"
      + " WHERE AMPHUR_SEQ = " + req.params.id).then(data => {
        return data.rows;
      });

    res.send(result);

  } catch (err) {
    res.send(err);
  } finally {
    if (connection) {
      try {
        await connection.close();
      } catch (err) {
        console.error(err);
      }
    }
  }

});

module.exports = router;