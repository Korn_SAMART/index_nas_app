var express = require("express");
var oracledb = require("oracledb");
var router = express.Router();

const SummaryCard = require('../models/summary_cards');
const ImageMapping = require('../models/mapping_images');
const IndexMapping = require('../models/mapping_indexes');
const MapMapping = require('../models/mapping_maps');
const ThreewaysMapping = require('../models/mapping_threeways');

const Top10_Error = require("../models/Top10_Error");
const Top10_Success = require("../models/Top10_Success");

oracledb.outFormat = oracledb.OBJECT;


router.get('/getSummaryCard', async (_, res) => {
  SummaryCard.find().lean().exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  });
})

router.get("/images", async (_, res) => {
  ImageMapping.find().lean().exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  });
})


router.get("/indexs", async (_, res) => {
  IndexMapping.find().lean().exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  });
})


router.get("/maps", async (_, res) => {
  MapMapping.find().lean().exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  });
})

router.get("/threeways", async (_, res) => {
  ThreewaysMapping.find().lean().exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  });
})

router.get("/top10error", async (_, res) => {
  Top10_Error.find().exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  })
})

router.get("/top10success", async (_, res) => {
  Top10_Success.find().exec((err, data) => {
    if (err) return res.status(400).send(err);
    res.status(200).send(data);
  })
})

router.get("/image_filter/", async (req, res) => {
  let imageMapping = ImageMapping.find();
  // if (req.query.region_seq) imageMapping = imageMapping.find({ region_seq: req.query.region_seq });
  if (req.query.province_seq) imageMapping = imageMapping.find({ province_seq: req.query.province_seq });

  imageMapping.exec((err, data) => {
    if (err) {
      console.log(err)
    }
    else {
      res.status(200).send(data)
    }
  });
});

router.get("/threeway_filter/", async (req, res) => {
  let threewayMapping = ThreewaysMapping.find();
  // if (req.query.region_seq) threewayMapping = threewayMapping.find({ region_seq: req.query.region_seq });
  if (req.query.province_seq) threewayMapping = threewayMapping.find({ province_seq: req.query.province_seq });

  threewayMapping.exec((err, data) => {
    if (err) {
      console.log(err)
    }
    else {
      res.status(200).send(data)
    }
  });
});


router.get("/index_filter/", async (_, res) => {
  let indexMapping = IndexMapping.find();

  indexMapping.exec((err, data) => {
    if (err) {
      console.log(err)
    }
    else {
      res.status(200).send(data)
    }
  });

});

router.get("/printplate_type_top/image", async (req, res) => {
  let { landoffice_seq } = req.query;
  let result, count = [0, 0, 0, 0, 0, 0, 0, 0, 0];

  if (landoffice_seq) {
    result = await ImageMapping.aggregate([
      {
        $match: {
          "LANDOFFICE_SEQ": landoffice_seq,
          "STS_UPD": {
            $ne: null
          }
        }
      },
      {
        $group: {
          _id: '$PRINTPLATE_TYPE_SEQ',
          count: { $sum: 1 }
        }
      },
      {
        $sort: {
          _id: 1
        }
      }
    ])
  }
  else {
    result = await ImageMapping.aggregate([
      {
        $match: {
          "STS_UPD": {
            $ne: null
          },
        }
      },
      {
        $group: {
          _id: '$PRINTPLATE_TYPE_SEQ',
          count: { $sum: 1 }
        }
      },
      {
        $sort: {
          _id: 1
        }
      }
    ])

  }

  for (let i = 0; i < result.length; i++) {
    count[Number(result[i]._id)] = result[i].count;
  }

  res.send([
    { y: count[0], label: "โฉนดที่ดิน", printplate_type_seq: 0, indexLabel: Number(count[0]).toLocaleString() },
    { y: count[1], label: "โฉนดตราจอง", printplate_type_seq: 2, indexLabel: Number(count[1]).toLocaleString() },
    { y: count[2], label: "ตราจองที่ได้ทำประโยชน์แล้ว", printplate_type_seq: 3, indexLabel: Number(count[2]).toLocaleString() },
    { y: count[3], label: "น.ส.3 ก", printplate_type_seq: 4, indexLabel: Number(count[3]).toLocaleString() },
    { y: count[4], label: "น.ส.3", printplate_type_seq: 5, indexLabel: Number(count[4]).toLocaleString() },
    { y: count[5], label: "น.ส.ล.", printplate_type_seq: 8, indexLabel: Number(count[5]).toLocaleString() },
    { y: count[6], label: "ห้องชุด", printplate_type_seq: 10, indexLabel: Number(count[6]).toLocaleString() },
    { y: count[7], label: "อาคารชุด", printplate_type_seq: 13, indexLabel: Number(count[7]).toLocaleString() },
    { y: count[8], label: "หนังสืออนุญาต", printplate_type_seq: 23, indexLabel: Number(count[8]).toLocaleString() },
  ].sort((a, b) => parseInt(a.y) - parseInt(b.y)))

});

router.get("/printplate_type_top/index", async (req, res) => {
  let { landoffice_seq } = req.query;
  let result, count = [0, 0, 0, 0, 0, 0, 0, 0, 0];

  if (landoffice_seq) {
    result = await IndexMapping.aggregate([
      {
        $match: {
          "LANDOFFICE_SEQ": landoffice_seq,
          "STS_UPD": {
            $ne: null
          }
        }
      },
      {
        $group: {
          _id: '$PRINTPLATE_TYPE_SEQ',
          count: { $sum: 1 }
        }
      },
      {
        $sort: {
          _id: 1
        }
      }
    ])
  }
  else {
    result = await IndexMapping.aggregate([
      {
        $match: {
          "STS_UPD": {
            $ne: null
          },
        }
      },
      {
        $group: {
          _id: '$PRINTPLATE_TYPE_SEQ',
          count: { $sum: 1 }
        }
      },
      {
        $sort: {
          _id: 1
        }
      }
    ])

  }

  for (let i = 0; i < result.length; i++) {
    count[Number(result[i]._id)] = result[i].count;
  }

  res.send([
    { y: count[0], label: "โฉนดที่ดิน", printplate_type_seq: 0, indexLabel: Number(count[0]).toLocaleString() },
    { y: count[1], label: "โฉนดตราจอง", printplate_type_seq: 2, indexLabel: Number(count[1]).toLocaleString() },
    { y: count[2], label: "ตราจองที่ได้ทำประโยชน์แล้ว", printplate_type_seq: 3, indexLabel: Number(count[2]).toLocaleString() },
    { y: count[3], label: "น.ส.3 ก", printplate_type_seq: 4, indexLabel: Number(count[3]).toLocaleString() },
    { y: count[4], label: "น.ส.3", printplate_type_seq: 5, indexLabel: Number(count[4]).toLocaleString() },
    { y: count[5], label: "น.ส.ล.", printplate_type_seq: 8, indexLabel: Number(count[5]).toLocaleString() },
    { y: count[6], label: "ห้องชุด", printplate_type_seq: 10, indexLabel: Number(count[6]).toLocaleString() },
    { y: count[7], label: "อาคารชุด", printplate_type_seq: 13, indexLabel: Number(count[7]).toLocaleString() },
    { y: count[8], label: "หนังสืออนุญาต", printplate_type_seq: 23, indexLabel: Number(count[8]).toLocaleString() },
  ].sort((a, b) => parseInt(a.y) - parseInt(b.y)))

});

module.exports = router;