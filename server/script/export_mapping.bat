@echo off

mongoimport D:\master\mapping_images.json -d dol -c mapping_images --drop --jsonArray && mongoimport D:\master\mapping_maps.json -d dol -c mapping_maps --drop --jsonArray && mongoimport D:\master\mapping_threeways.json -d dol -c mapping_threeways --drop --jsonArray && mongoimport D:\master\mapping_image_noneupdate.json -d dol -c mapping_images --jsonArray
