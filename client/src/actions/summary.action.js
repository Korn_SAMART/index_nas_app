import { SUMMARY_FAILED, SUMMARY_FETCHING, SUMMARY_SUCCESS } from "../constant"

export const setSummaryFetchingToState = () => ({
  type: SUMMARY_FETCHING
})

export const setSummarySuccessToState = (payload) => ({
  type: SUMMARY_SUCCESS,
  payload
})

export const setSummaryFailedToState = (payload) => ({
  type: SUMMARY_FAILED,
  payload
})
export const getSummary = (printplate_type_seq) => {
  return async (dispatch) => {
    try {
      dispatch(setSummaryFetchingToState());
      let url = 'http://localhost:9998/api/dashboard/summary?printplate_type_seq=' + printplate_type_seq;

      let result = await fetch(url).then(res => {
        return res.json();
      })

      dispatch(setSummarySuccessToState(result));
    } catch (error) {
      dispatch(setSummaryFailedToState(JSON.stringify(error)));
    }
  }
}
