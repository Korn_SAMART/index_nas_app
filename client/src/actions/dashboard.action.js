import {
  DASHBOARD_CLEARED,
  DASHBOARD_FAILED,
  DASHBOARD_FETCHING,
  DASHBOARD_FILTER_SUCCESS,
  DASHBOARD_SUCCESS,
} from "../constant";

export const setDashboardFetchingToState = () => ({
  type: DASHBOARD_FETCHING,
});

export const setDashboardSuccessToState = (payload) => ({
  type: DASHBOARD_SUCCESS,
  payload,
});

export const setDashboardFilterSuccess = (payload) => ({
  type: DASHBOARD_FILTER_SUCCESS,
  payload
})


export const setDashboardFailedToState = (payload) => ({
  type: DASHBOARD_FAILED,
  payload,
});

export const setDashboardClearedToState = (payload) => ({
  type: DASHBOARD_CLEARED,
  payload,
});

export const initialDashboard = () => {
  return async (dispatch) => {
    try {
      dispatch(setDashboardFetchingToState());
      //summary
      let url = "http://localhost:9998/api/dashboard/getSummaryCard";
      let summary = await fetch(url).then((res) => {
        return res.json();
      });


      // images
      url = "http://localhost:9998/api/dashboard/images";
      let images = await fetch(url).then((res) => {
        return res.json();
      });

      // indexs
      url = "http://localhost:9998/api/dashboard/indexs";
      let indexs = await fetch(url).then((res) => {
        return res.json();
      });

      // maps
      url = "http://localhost:9998/api/dashboard/maps";
      let maps = await fetch(url).then((res) => {
        return res.json();
      });

      // threeways
      url = "http://localhost:9998/api/dashboard/threeways";
      let threeways = await fetch(url).then((res) => {
        return res.json();
      });

      // top10success
      url = "http://localhost:9998/api/dashboard/top10success";
      let top10_successes = await fetch(url).then(res => res.json()).then(data => {
        let result = [];
        for (let i = 0; i < data.length; i++) {
          result.push({ y: data[i].COUNT, label: data[i].LANDOFFICE_NAME_TH, landoffice_seq: data[i].LANDOFFICE_SEQ, indexLabel: Number(data[i].COUNT).toLocaleString(), type: data[i].TYPE })
        }
        return result;
      })

      // top10error
      url = "http://localhost:9998/api/dashboard/top10error";
      let top10_errors = await fetch(url).then(res => res.json()).then(data => {
        let result = [];
        for (let i = 0; i < data.length; i++) {
          result.push({ y: data[i].COUNT, label: data[i].LANDOFFICE_NAME_TH, landoffice_seq: data[i].LANDOFFICE_SEQ, indexLabel: Number(data[i].COUNT).toLocaleString(), type: data[i].TYPE })
        }
        return result;
      })


      dispatch(
        setDashboardSuccessToState({
          image: images,
          index: indexs,
          mapdol: maps,
          threeway: threeways,
          reg_count: summary[0].REG_COUNT,
          image_count: summary[0].IMG_COUNT,
          index_count: summary[0].INDEX_COUNT,
          mapdol_count: summary[0].MAPDOL_COUNT,
          top10_success: {
            image: top10_successes.filter(data => data.type == 'image_match').sort((a, b) => a.y - b.y),
            index: top10_successes.filter(data => data.type == 'index_match').sort((a, b) => a.y - b.y),
            mapdol: top10_successes.filter(data => data.type == 'map_match').sort((a, b) => a.y - b.y),
            threeway: top10_successes.filter(data => data.type == 'three_match').sort((a, b) => a.y - b.y)
          },
          top10_error: {
            image: top10_errors.filter(data => data.type == 'image_unmatch').sort((a, b) => a.y - b.y),
            index: top10_errors.filter(data => data.type == 'index_unmatch').sort((a, b) => a.y - b.y),
            mapdol: top10_errors.filter(data => data.type == 'map_unmatch').sort((a, b) => a.y - b.y),
            threeway: top10_errors.filter(data => data.type == 'three_unmatch').sort((a, b) => a.y - b.y)
          },
          top10_noneupdate: {
            image: top10_errors.filter(data => data.type == 'image_noneupdate').sort((a, b) => a.y - b.y)
          },
          overall_image: images,
          overall_index: indexs,
          overall_mapdol: maps,
          overall_threeway: threeways,
        })
      );
    } catch (error) {
      dispatch(setDashboardFailedToState(JSON.stringify(error)));
    }
  };
};

export const restoreDashboard = () => {
  return async (dispatch, getState) => {
    try {
      dispatch(setDashboardFetchingToState())
      dispatch(setDashboardClearedToState({
        image: getState().dashboardReducer.overall_image,
        index: getState().dashboardReducer.overall_index,
        mapdol: getState().dashboardReducer.overall_mapdol,
        threeway: getState().dashboardReducer.overall_threeway
      }));
    } catch (error) {
      dispatch(setDashboardFailedToState(JSON.stringify(error)));
    }
  };
};


export const getSummaryByLandofficeSeq = (landoffice_seq) => {
  return async (dispatch) => {
    try {
      dispatch(setDashboardFetchingToState());

      let url = 'http://localhost:9998/api/dashboard?landoffice_seq=' + landoffice_seq;
      let result = await fetch(url).then((res) => {
        return res.json();
      })

      dispatch(setDashboardFilterSuccess({

      }))
    } catch (error) {
      dispatch(setDashboardFailedToState(JSON.stringify(error)))
    }
  }
}

export const getDashboard = (body) => {
  return async (dispatch) => {
    try {
      dispatch(setDashboardFetchingToState());

      let url = `http://localhost:9998/api/dashboard/image_filter?region_seq=${body.region_seq != 'default' ? body.region_seq : 0}&province_seq=${body.province_seq != 'default' ? body.province_seq : 0}`;
      let image = await fetch(url).then((res) => {
        return res.json();
      });

      url = `http://localhost:9998/api/dashboard/index_filter?region_seq=${body.region_seq != 'default' ? body.region_seq : 0}&province_seq=${body.province_seq != 'default' ? body.province_seq : 0}`;
      let index = await fetch(url).then((res) => {
        return res.json();
      });

      dispatch(
        setDashboardFilterSuccess({
          image: image,
          index: index
        })
      );


    } catch (error) {
      dispatch(setDashboardFailedToState(JSON.stringify(error)));
    }
  };
};
