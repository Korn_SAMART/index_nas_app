import {
  PRINTPLATE_TOP_CLEARED,
  PRINTPLATE_TOP_FAILED,
  PRINTPLATE_TOP_FETCHING,
  PRINTPLATE_TOP_SUCCESS,
} from "../constant";

export const setPrintplate_TopFetchingToState = () => ({
  type: PRINTPLATE_TOP_FETCHING,
});

export const setPrintplate_TopSuccessToState = (payload, datatype) => ({
  type: PRINTPLATE_TOP_SUCCESS,
  payload,
  datatype,
});

export const setPrintplate_TopFailedToState = (payload) => ({
  type: PRINTPLATE_TOP_FAILED,
  payload,
});

export const setPrintplate_TopClearedToState = () => ({
  type: PRINTPLATE_TOP_CLEARED,
});

export const getPrintplate_Top = (landoffice_seq, type) => {
  return async (dispatch) => {
    try {
      dispatch(setPrintplate_TopFetchingToState());
      let url = "http://localhost:9998/api/dashboard/printplate_type_top/" + type;
      if (landoffice_seq) url += "?landoffice_seq=" + landoffice_seq;

      let result = await fetch(url).then((res) => {
        return res.json();
      });

      dispatch(setPrintplate_TopSuccessToState(result, type));
    } catch (error) {
      dispatch(setPrintplate_TopFailedToState(JSON.stringify(error)));
    }
  };
};
