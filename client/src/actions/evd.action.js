import { EVD_FAILED, EVD_FETCHING, EVD_SUCCESS } from '../constant';

export const setEVDFetchingToState = () => ({
  type: EVD_FETCHING
})

export const setEVDSuccessToState = (payload) => ({
  type: EVD_SUCCESS,
  payload
})

export const setEVDFailedToState = () => ({
  type: EVD_FAILED,
})


export const displayParcelImage = (parcel_seq, printplate_type_seq) => {
  return async (dispatch) => {
    try {
      dispatch(setEVDFetchingToState());
      let url = 'http://localhost:9998/api/search/image_list/' + parcel_seq + '?printplate_type_seq=' + printplate_type_seq
      const result = await fetch(url).then(res => {
        return res.json();
      })

      dispatch(setEVDSuccessToState(result));

    } catch (error) {
      dispatch(setEVDFailedToState(JSON.stringify(error)));
    }
  }
}

export const displayParcelHsfs = (parcel_seq, printplate_type_seq) => {
  return async (dispatch) => {
    try {
      dispatch(setEVDFetchingToState());
      let url = 'http://localhost:9998/api/search/hsfs_list/' + parcel_seq + '?printplate_type_seq=' + printplate_type_seq
      const result = await fetch(url).then(res => {
        return res.json();
      })
      dispatch(setEVDSuccessToState(result));

    } catch (error) {
      dispatch(setEVDFailedToState(JSON.stringify(error)));
    }
  }
}

