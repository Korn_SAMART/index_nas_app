import { PROVINCE_FAILED, PROVINCE_FETCHING, PROVINCE_SUCCESS, REGION_FAILED, REGION_FETCHING, REGION_SUCCESS } from "../constant";

export const setProvincesFetchingToState = () => ({
  type: PROVINCE_FETCHING
})

export const setProvincesSuccessToState = (payload) => ({
  type: PROVINCE_SUCCESS,
  payload
})

export const setProvincesFailedToState = (payload) => ({
  type: PROVINCE_FAILED,
  payload
})

export const setRegionFetchingToState = () => ({
  type: REGION_FETCHING
})

export const setRegionSuccessToState = (payload) => ({
  type: REGION_SUCCESS,
  payload
})

export const setRegionFailedToState = (payload) => ({
  type: REGION_FAILED,
  payload
})

export const getProvinces = () => {
  return async (dispatch) => {
    try {
      dispatch(setProvincesFetchingToState());

      let result = await fetch("http://localhost:9998/api/dropdown/province").then((res) => {
        return res.json();
      });

      dispatch(setProvincesSuccessToState(result));

    } catch (error) {
      dispatch(setProvincesFailedToState(JSON.stringify(error)));
    }
  }
}

export const getRegion = () => {
  return async (dispatch) => {
    try {
      dispatch(setRegionFetchingToState());

      let result = await fetch("http://localhost:9998/api/dropdown/region").then((res) => {
        return res.json();
      })

      dispatch(setRegionSuccessToState(result));

    } catch (error) {
      dispatch(setRegionFailedToState(JSON.stringify(error)));
    }
  }
}


