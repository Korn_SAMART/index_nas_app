import { IMAGE_FETCHING, IMAGE_SUCCESS, IMAGE_FAILED, IMAGE_CLEARED } from '../constant';

export const setImageFetchingToState = () => ({
  type: IMAGE_FETCHING
})

export const setImageSuccessToState = (payload) => ({
  type: IMAGE_SUCCESS,
  payload
})

export const setImageSuccessByLandofficeSeqToState = (payload, landoffice_seq) => ({
  type: IMAGE_SUCCESS,
  payload,
  landoffice_seq
})

export const setImageSuccessByPrintplateTypeSeqToState = (payload, printplate_type_seq) => ({
  type: IMAGE_SUCCESS,
  payload,
  printplate_type_seq
})

export const setImageFailedToState = (payload) => ({
  type: IMAGE_FAILED,
  payload
})

export const clearImageToState = () => ({
  type: IMAGE_CLEARED
})

export const search = (body) => {
  return async (dispatch) => {
    try {
      dispatch(setImageFetchingToState());
      let url = 'http://localhost:9998/api/search?landoffice_seq=' + body.landoffice_seq;
      localStorage.setItem('landoffice_seq', body.landoffice_seq);
      if (body.start_seq) url += '&start_seq=' + body.start_seq;
      if (body.end_seq) url += '&end_seq=' + body.end_seq;
      if (body.amphur_seq && body.amphur_seq != 'เลือกอำเภอ') url += '&amphur_seq=' + body.amphur_seq;
      if (body.tambol_seq && body.tambol_seq != 'เลือกตำบล') url += '&tambol_seq=' + body.tambol_seq;
      if (body.printplate_type_seq) url += '&printplate_type_seq=' + body.printplate_type_seq;
      if (body.search_type) url += '&searchType=' + body.search_type

      let result = await fetch(url).then(res => {
        return res.json();
      })

      dispatch(setImageSuccessToState(result));

    } catch (error) {
      dispatch(setImageFailedToState(JSON.stringify(error)));
    }
  }
}

export const getImageDetailByPrintplateTypeSeq = (printplate_type_seq) => {
  return async (dispatch, getState) => {
    try {
      dispatch(setImageFetchingToState());

      let result = getState().dashboardReducer.image.filter(img => img.STS_UPD != null && img.PRINTPLATE_TYPE_SEQ == printplate_type_seq);

      dispatch(setImageSuccessByPrintplateTypeSeqToState(result, printplate_type_seq));

    } catch (error) {
      dispatch(setImageFailedToState(JSON.stringify(error)));
    }
  }
}

export const getImageDetailByLandoffice = (landoffice_seq, printplate_type_seq) => {
  return async (dispatch) => {
    try {
      localStorage.setItem('landoffice_seq', landoffice_seq);
      dispatch(setImageFetchingToState());
      let url = 'http://localhost:9998/api/dashboard/summary/' + landoffice_seq + '?printplate_type_seq=' + printplate_type_seq;

      let result = await fetch(url).then(res => {
        return res.json();
      })

      dispatch(setImageSuccessByLandofficeSeqToState(result, landoffice_seq));

    } catch (error) {
      dispatch(setImageFailedToState(JSON.stringify(error)));
    }
  }
}