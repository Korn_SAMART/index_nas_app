import { ThemeProvider, createTheme, styled } from "@mui/material/styles";
import {
  HashRouter as Router,
  Route,
  Switch,
  Link
} from "react-router-dom";
import { useSelector } from "react-redux";
import Box from '@mui/material/Box';
import Drawer from '@mui/material/Drawer';
import CssBaseline from '@mui/material/CssBaseline';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import React, { useState } from "react";
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import ImagePage from './components/pages/ImagePage/ImagePage';
import SummaryPage from "./components/pages/SummaryPage";
import MapdolPage from "./components/pages/MapdolPage";
import logo from './images/dol.png';
import IndexPage from "./components/pages/IndexPage/IndexPage";

const drawerWidth = 350;

const Main = styled('main', { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    flexGrow: 1,
    padding: theme.spacing(3),
    transition: theme.transitions.create('margin', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    marginLeft: `-${drawerWidth}px`,
    ...(open && {
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
    }),
  }),
);

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  transition: theme.transitions.create(['margin', 'width'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: `${drawerWidth}px`,
    transition: theme.transitions.create(['margin', 'width'], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const DrawerHeader = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: 'flex-end',
}));


const theme = createTheme({
  palette: {
    background: {
      default: "#FFFFFF",
    },
  },
});

export default function App() {
  const [toggleKey, setToggleKey] = useState(0);
  const dashboardReducer = useSelector((state) => state.dashboardReducer);


  return (
    <ThemeProvider theme={theme}>
      <Router>
        <Box className="main" sx={{ display: 'flex', minHeight: '100vh', background: '#0f6674' }}>
          <CssBaseline />
          <AppBar style={{ backgroundColor: '#001529' }} position="fixed">
            <Toolbar>
              <img src={logo} width={80} alt='dol-logo' />
              <Typography fontFamily="Kanit" variant="h5" noWrap component="div" marginLeft={2}>
                ระบบรายงานตรวจสอบข้อมูลสารสนเทศที่ดิน
              </Typography>
            </Toolbar>
          </AppBar>

          <Drawer
            sx={{
              width: drawerWidth,
              flexShrink: 0,
              '& .MuiDrawer-paper': {
                width: drawerWidth,
                boxSizing: 'border-box',
              },
            }}
            variant="persistent"
            anchor="left"
          >
            <Divider />
          </Drawer>
          <Main>
            <DrawerHeader />
           {dashboardReducer.image && <div style={{ display: "flex", flexDirection: "column", rowGap: "20px" }}>
              <ul className="toggle-button">
                <li onClick={() => setToggleKey(0)} className={toggleKey == 0 ? 'active' : ''}><Link to='/'><label>ภาพรวม</label></Link></li>
                <li onClick={() => setToggleKey(1)} className={toggleKey == 1 ? 'active' : ''}><Link to='/image'><label>ภาพลักษณ์</label></Link></li>
                {/* <li onClick={() => setToggleKey(2)} className={toggleKey == 2 ? 'active' : ''}><Link to='/index'><label>สารบบ</label></Link></li> */}
                <li onClick={() => setToggleKey(3)} className={toggleKey == 3 ? 'active' : ''}><Link to='/mapdol'><label>รูปแปลง</label></Link></li>
              </ul>
            </div>}
            <Switch>
              <Route exact path='/' component={SummaryPage} />
              <Route path='/image' component={ImagePage} />
              {/* <Route path='/index' component={IndexPage} /> */}
              <Route path='/mapdol' component={MapdolPage} />
            </Switch>
          </Main>
        </Box>
      </Router>
    </ThemeProvider>
  );
}