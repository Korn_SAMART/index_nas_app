export const getImageUnmatched = () => {
  return (dispatch, getState) => {
    // grab current state
    const state = getState();
    const { dashboardReducer } = state;

    return [{
      actual: dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '1').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '1').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '1').length)
      ) * 100),
      label: 'โฉนดที่ดิน',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '1').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '1').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '2').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '2').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '2').length)
      ) * 100),
      label: 'โฉนดตราจอง',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '2').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '2').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '3').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '3').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '3').length)
      ) * 100),
      label: 'ตราจองที่ได้ทำประโยชน์แล้ว',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '3').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '3').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '4').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '4').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '4').length)
      ) * 100),
      label: 'น.ส.3ก',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '4').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '4').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '5').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '5').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '5').length)
      ) * 100),
      label: 'น.ส.3',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '5').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '5').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '8').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '8').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '8').length)
      ) * 100),
      label: 'น.ส.ล.',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '8').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '8').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '10').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '10').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '10').length)
      ) * 100),
      label: 'ห้องชุด',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '10').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '10').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '13').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '13').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '13').length)
      ) * 100),
      label: 'อาคารชุด',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '13').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '13').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '23').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '23').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '23').length)
      ) * 100),
      label: 'หนังสืออนุญาต',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '23').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '23').length)
      ) * 100).toFixed(2) + ' %'
    }]
  }
}

export const getImageNoneUpdate = () => {
  return (dispatch, getState) => {
    // grab current state
    const state = getState();
    const { dashboardReducer } = state;

    return [{
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '1').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '1').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '1').length)
      ) * 100),
      label: 'โฉนดที่ดิน',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '1').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '1').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '2').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '2').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '2').length)
      ) * 100),
      label: 'โฉนดตราจอง',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '2').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '2').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '3').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '3').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '3').length)
      ) * 100),
      label: 'ตราจองที่ได้ทำประโยชน์แล้ว',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '3').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '3').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '4').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '4').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '4').length)
      ) * 100),
      label: 'น.ส.3ก',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '4').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '4').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '5').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '5').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '5').length)
      ) * 100),
      label: 'น.ส.3',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '5').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '5').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '8').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '8').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '8').length)
      ) * 100),
      label: 'น.ส.ล.',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '8').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '8').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '10').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '10').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '10').length)
      ) * 100),
      label: 'ห้องชุด',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '10').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '10').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '13').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '13').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '13').length)
      ) * 100),
      label: 'อาคารชุด',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '13').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '13').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '23').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '23').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '23').length)
      ) * 100),
      label: 'หนังสืออนุญาต',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '23').length)

        / (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '23').length)
      ) * 100).toFixed(2) + ' %'
    }];
  }
}

export const getSummaryThreewaySuccess = () => {
  return (dispatch, getState) => {
    // grab current state
    const state = getState();
    const { dashboardReducer } = state;

    return [{
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length) * 100,
      label: 'โฉนดที่ดิน',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length) * 100,
      label: 'โฉนดตราจอง',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length) * 100,
      label: 'ตราจองที่ได้ทำประโยชน์แล้ว',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length) * 100,
      label: 'น.ส.3ก',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length) * 100,
      label: 'น.ส.3',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length) * 100,
      label: 'น.ส.ล.',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length) * 100,
      label: 'ห้องชุด',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length) * 100,
      label: 'อาคารชุด',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length) * 100,
      label: 'หนังสืออนุญาต',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23' && (i.UTMMAP1 != null || i.UTMMAP3 != null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length) * 100).toFixed(2) + ' %',
    }];
  }
}

export const getSummaryImageSuccess = () => {
  return (dispatch, getState) => {
    // grab current state
    const state = getState();
    const { dashboardReducer } = state;

    return [{
      actual: (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '1').length
        + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '1').length),
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '1').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length)
      ) * 100),
      label: 'โฉนดที่ดิน',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '1').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '2').length
        + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '2').length),
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '2').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length)
      ) * 100),
      label: 'โฉนดตราจอง',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '2').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '3').length
        + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '3').length),
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '3').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length)
      ) * 100),
      label: 'ตราจองที่ได้ทำประโยชน์แล้ว',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '3').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '4').length
        + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '4').length),
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '4').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length)
      ) * 100),
      label: 'น.ส.3ก',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '4').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '5').length
        + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '5').length),
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '5').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length)
      ) * 100),
      label: 'น.ส.3',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '5').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '8').length
        + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '8').length),
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '8').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length)
      ) * 100),
      label: 'น.ส.ล.',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '8').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '10').length
        + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '10').length),
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '10').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length)
      ) * 100),
      label: 'ห้องชุด',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '10').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '13').length
        + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '13').length),
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '13').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length)
      ) * 100),
      label: 'อาคารชุด',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '13').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '23').length
        + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '23').length),
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '23').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length)
      ) * 100),
      label: 'หนังสืออนุญาต',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '23').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length)
      ) * 100).toFixed(2) + ' %'
    }];
  }
}

export const getSummaryIndexSuccess = () => {
  return (dispatch, getState) => {
    // grab current state
    const state = getState();
    const { dashboardReducer } = state;

    return [{
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '1').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length)
      ) * 100),
      label: 'โฉนดที่ดิน',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '1').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '2').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length)
      ) * 100),
      label: 'โฉนดตราจอง',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '2').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '3').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length)
      ) * 100),
      label: 'ตราจองที่ได้ทำประโยชน์แล้ว',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '3').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '4').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length)
      ) * 100),
      label: 'น.ส.3ก',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '4').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '5').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length)
      ) * 100),
      label: 'น.ส.3',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '5').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '8').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length)
      ) * 100),
      label: 'น.ส.ล.',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '8').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '10').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length)
      ) * 100),
      label: 'ห้องชุด',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '10').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '13').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length)
      ) * 100),
      label: 'อาคารชุด',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '13').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '23').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length)
      ) * 100),
      label: 'หนังสืออนุญาต',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD == null && i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ != null && i.PRINTPLATE_TYPE_SEQ == '23').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length)
      ) * 100).toFixed(2) + ' %'
    }];
  }
}

export const getSummaryMapdolSuccess = () => {
  return (dispatch, getState) => {
    // grab current state
    const state = getState();
    const { dashboardReducer } = state;

    return [{
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '1' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '1' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '1').length) * 100,
      label: 'โฉนดที่ดิน',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '1' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '1').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '2' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '2' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '2').length) * 100,
      label: 'โฉนดตราจอง',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '2' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '2').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '3' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '3' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '3').length) * 100,
      label: 'ตราจองที่ได้ทำประโยชน์แล้ว',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '3' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '3').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '4' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '4' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '4').length) * 100,
      label: 'น.ส. 3ก',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '4' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '4').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '5' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '5' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '5').length) * 100,
      label: 'น.ส.3',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '5' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '5').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '8' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '8' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '8').length) * 100,
      label: 'น.ส.ล.',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '8' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '8').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '10' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '10' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '10').length) * 100,
      label: 'ห้องชุด',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '10' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '10').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '13' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '13' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '13').length) * 100,
      label: 'อาคารชุด',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '13' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '13').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '23' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '23' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '23').length) * 100,
      label: 'หนังสืออนุญาต',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '23' && (m.UTMMAP1 != null || m.UTMMAP3 != null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '23').length)
        * 100).toFixed(2) + ' %'
    }];
  }
}

export const getSummaryThreewayFailed = () => {
  return (dispatch, getState) => {
    // grab current state
    const state = getState();
    const { dashboardReducer } = state;

    return [{
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length) * 100,
      label: 'โฉนดที่ดิน',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length) * 100,
      label: 'โฉนดตราจอง',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length) * 100,
      label: 'ตราจองที่ได้ทำประโยชน์แล้ว',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length) * 100,
      label: 'น.ส.3ก',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length) * 100,
      label: 'น.ส.3',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length) * 100,
      label: 'น.ส.ล.',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length) * 100,
      label: 'ห้องชุด',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length) * 100,
      label: 'อาคารชุด',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length) * 100).toFixed(2) + ' %',
    }, {
      actual: dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length,
      y: (dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length) * 100,
      label: 'หนังสืออนุญาต',
      indexLabel: ((dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23' && (i.UTMMAP1 == null || i.UTMMAP3 == null)).length
        / dashboardReducer.threeway.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length) * 100).toFixed(2) + ' %',
    }];
  }
}

export const getSummaryImageFailed = () => {
  return (dispatch, getState) => {
    // grab current state
    const state = getState();
    const { dashboardReducer } = state;

    return [{
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '1').length
        + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '1').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '1').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length)
      ) * 100),
      label: 'โฉนดที่ดิน',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '1').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '2').length
      + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '2').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '2').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length)
      ) * 100),
      label: 'โฉนดตราจอง',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '2').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '3').length
      + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '3').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '3').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length)
      ) * 100),
      label: 'ตราจองที่ได้ทำประโยชน์แล้ว',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '3').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '4').length
      + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '4').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '4').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length)
      ) * 100),
      label: 'น.ส.3ก',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '4').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '5').length
      + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '5').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '5').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length)
      ) * 100),
      label: 'น.ส.3',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '5').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '8').length
      + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '8').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '8').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length)
      ) * 100),
      label: 'น.ส.ล.',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '8').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '10').length
      + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '10').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '10').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length)
      ) * 100),
      label: 'ห้องชุด',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '10').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '13').length
      + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '13').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '13').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length)
      ) * 100),
      label: 'อาคารชุด',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '13').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '23').length
      + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '23').length,
      y: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '23').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length)
      ) * 100),
      label: 'หนังสืออนุญาต',
      indexLabel: ((
        (dashboardReducer.image.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '23').length)

        / (dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.image.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length)
      ) * 100).toFixed(2) + ' %'
    }];
  }
}

export const getSummaryIndexFailed = () => {
  return (dispatch, getState) => {
    // grab current state
    const state = getState();
    const { dashboardReducer } = state;

    return [{
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '1').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length)
      ) * 100),
      label: 'โฉนดที่ดิน',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '1').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '1').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '2').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length)
      ) * 100),
      label: 'โฉนดตราจอง',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '2').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '2').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '3').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length)
      ) * 100),
      label: 'ตราจองที่ได้ทำประโยชน์แล้ว',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '3').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '3').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '4').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length)
      ) * 100),
      label: 'น.ส.3ก',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '4').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '4').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '5').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length)
      ) * 100),
      label: 'น.ส.3',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '5').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '5').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '8').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length)
      ) * 100),
      label: 'น.ส.ล.',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '8').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '8').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '10').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length)
      ) * 100),
      label: 'ห้องชุด',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '10').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '10').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '13').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length)
      ) * 100),
      label: 'อาคารชุด',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '13').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '13').length)
      ) * 100).toFixed(2) + ' %'
    },
    {
      y: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '23').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length)
      ) * 100),
      label: 'หนังสืออนุญาต',
      indexLabel: ((
        (dashboardReducer.index.filter((i) => i.STS_UPD != null && i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.index.filter((i) => i.PARCEL_SEQ == null && i.PRINTPLATE_TYPE_SEQ == '23').length)

        / (dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length
          + dashboardReducer.index.filter((i) => i.PRINTPLATE_TYPE_SEQ == '23').length)
      ) * 100).toFixed(2) + ' %'
    }];
  }
}

export const getSummaryMapdolFailed = () => {
  return (dispatch, getState) => {
    // grab current state
    const state = getState();
    const { dashboardReducer } = state;

    return [{
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '1' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '1' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '1').length) * 100,
      label: 'โฉนดที่ดิน',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '1' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '1').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '2' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '2' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '2').length) * 100,
      label: 'โฉนดตราจอง',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '2' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '2').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '3' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '3' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '3').length) * 100,
      label: 'ตราจองที่ได้ทำประโยชน์แล้ว',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '3' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '3').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '4' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '4' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '4').length) * 100,
      label: 'น.ส.3ก',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '4' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '4').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '5' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '5' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '5').length) * 100,
      label: 'น.ส.3',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '5' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '5').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '8' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '8' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '8').length) * 100,
      label: 'น.ส.ล.',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '8' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '8').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '10' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '10' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '10').length) * 100,
      label: 'ห้องชุด',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '10' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '10').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '13' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '13' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '13').length) * 100,
      label: 'อาคารชุด',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '13' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '13').length)
        * 100).toFixed(2) + ' %'
    },
    {
      actual: dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '23' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length,
      y: (dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '23' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '23').length) * 100,
      label: 'หนังสืออนุญาต',
      indexLabel: ((dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '23' && (m.UTMMAP1 == null || m.UTMMAP3 == null)).length / dashboardReducer.mapdol.filter((m) => m.PRINTPLATE_TYPE_SEQ == '23').length)
        * 100).toFixed(2) + ' %'
    }];
  }
}

