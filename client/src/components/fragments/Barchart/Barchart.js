import React from "react";
import { useSelector } from "react-redux";
import CanvasJSReact from "../../../assets/canvasjs.react";
import './Barchart.css';

var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

CanvasJS.addColorSet("greenShades", [
  //colorSet Arra
  "#CEFAD0",
  "#CEFAD0",
  "#ABF7B1",
  "#83F28F",
  "#5CED73",
  "#39E75F",
  "#1FD655",
  "#00C04B",
  "#00AB41",
  "#008631",
]);

CanvasJS.addColorSet("redShades", [
  //colorSet Array
  "#FFCBD1",
  "#FFCBD1",
  "#F69697",
  "#EE6B6E",
  "#F94449",
  "#FF2C2C",
  "#F01E2C",
  "#DE0A26",
  "#D1001F",
  "#C30010",
]);

export default function Barchart({ landoffice_seq, top10, isSuccess, title, datatype }) {
  const printplate_topReducer = useSelector(
    (state) => state.printplate_topReducer
  );

  return (
    <>
      {top10 && top10.length > 0 && <CanvasJSChart
        options={{
          colorSet: isSuccess ? "greenShades" : "redShades",
          animationEnabled: true,
          backgroundColor: "#2b2f34",
          legend: {
            fontColor: "white",
          },
          title: {
            text: title,
            fontColor: 'white',
            fontFamily: 'Kanit',
            fontSize: 22
          },
          axisX: {
            labelFontColor: "white",
            labelFontFamily: "Kanit",
            labelFontSize: 15,
            labelMaxWidth: 1500,

          },
          axisY: {
            labelFontColor: "white",
            labelFontFamily: "Kanit",
            labelFontSize: 18,
          },
          data: [
            {
              type: "bar",
              indexLabelFontSize: 25,
              indexLabelFontFamily: "Kanit",
              indexLabelFontWeight: 500,
              indexLabelFontColor: "white",
              dataPoints: top10,
            },
          ],

        }}
      />}


      {landoffice_seq &&
        <CanvasJSChart
          options={{
            colorSet: "greenShades",
            animationEnabled: true,
            backgroundColor: "#2b2f34",
            legend: {
              fontColor: "white",
            },
            axisX: {
              labelFontColor: "white",
              labelFontFamily: "Kanit",
              labelFontSize: 18,

            },
            axisY: {
              labelFontColor: "white",
              labelFontFamily: "Kanit",
              labelFontSize: 18,
            },
            data: [
              {
                type: "bar",
                indexLabelFontSize: 25,
                indexLabelFontFamily: "Kanit",
                indexLabelFontWeight: 500,
                indexLabelFontColor: "white",
                dataPoints: printplate_topReducer.result,
              },
            ],
          }}
        />
     
      }
    </>
  );
}
