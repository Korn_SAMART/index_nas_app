import React from "react";
import { useDispatch } from "react-redux";
import CanvasJSReact from "../../../assets/canvasjs.react";
import { getImageUnmatched, getImageNoneUpdate } from "../../../utils";

var CanvasJSChart = CanvasJSReact.CanvasJSChart;

export default function ColumnChart({ title, datatype }) {

  const dispatch = useDispatch();

  return (
    <>
      <CanvasJSChart
        options={{
          animationEnabled: true,
          backgroundColor: "#2b2f34",
          legend: {
            fontColor: "white",
          },
          title: {
            text: title,
            fontColor: 'white',
            fontFamily: 'Kanit',
            fontSize: 22
          },
          toolTip:{   
            content: "{label}: <b>{actual}</b> เอกสารสิทธิ",
            fontSize: 18,
            fontFamily: 'Kanit'
          },
          axisX: {
            labelFontColor: "white",
            labelFontFamily: "Kanit",
            labelFontSize: 15,
            labelMaxWidth: 1500,

          },
          axisY: {
            labelFontColor: "white",
            labelFontFamily: "Kanit",
            labelFontSize: 15,
            minimum: 0,
            maximum: 100,
            interval: 20
          },
          data: [
            {
              type: "column",
              color: '#dc3545',
              indexLabelFontSize: 18,
              indexLabelFontFamily: "Kanit",
              indexLabelFontWeight: 500,
              indexLabelFontColor: "white",
              dataPoints: datatype.includes('image_unmatched') ? dispatch(getImageUnmatched()) : dispatch(getImageNoneUpdate())

            },
          ],

        }}
      />



    </>
  );
}
