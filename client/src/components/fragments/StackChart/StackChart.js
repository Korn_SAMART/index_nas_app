import React from "react";
import { useDispatch } from "react-redux";
import CanvasJSReact from "../../../assets/canvasjs.react";
import {
  getSummaryImageFailed, getSummaryImageSuccess,
  getSummaryIndexFailed, getSummaryIndexSuccess, getSummaryMapdolFailed,
  getSummaryMapdolSuccess, getSummaryThreewayFailed, getSummaryThreewaySuccess
} from "../../../utils";


var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;


export default function StackChart({ datatype, title }) {
  const dispatch = useDispatch();

  return (
    <>
      <CanvasJSChart
        options={{
          animationEnabled: true,
          backgroundColor: "#2b2f34",
          legend: {
            fontColor: "white",
          },
          title: {
            text: title,
            fontColor: 'white',
            fontFamily: 'Kanit',
            fontSize: 22
          },
          toolTip: {
            content: "{label}: <b>{actual}</b> เอกสารสิทธิ",
            fontSize: 18,
            fontFamily: 'Kanit'
          },
          axisX: {
            labelFontColor: "white",
            labelFontFamily: "Kanit",
            labelFontSize: 15,
            labelMaxWidth: 1500,

          },
          axisY: [{
            labelFontColor: "white",
            labelFontFamily: "Kanit",
            labelFontSize: 18,
            includeZero: true,
            maximum: 100
          }, {
          }],
          data: [
            {
              type: "stackedColumn100",
              color: "#28a745",
              showInLegend: false,
              indexLabelFontColor: "white",
              indexLabelFontSize: 18,
              indexLabelFontFamily: "Kanit",
              indexLabelPlacement: "inside",
              indexLabelMaxWidth: 150,
              dataPoints: datatype == 'summary_three' ? dispatch(getSummaryThreewaySuccess()) :
                datatype == 'summary_image' ? dispatch(getSummaryImageSuccess()) :
                  datatype == 'summary_index' ? dispatch(getSummaryIndexSuccess()) :
                    dispatch(getSummaryMapdolSuccess()),

            },
            {
              type: "stackedColumn100",
              color: "#dc3545",
              showInLegend: false,
              indexLabelFontColor: "white",
              indexLabelFontSize: 18,
              indexLabelFontFamily: "Kanit",
              indexLabelPlacement: "inside",
              indexLabelMaxWidth: 150,
              dataPoints: datatype == 'summary_three' ? dispatch(getSummaryThreewayFailed()) :
                datatype == 'summary_image' ? dispatch(getSummaryImageFailed()) :
                  datatype == 'summary_index' ? dispatch(getSummaryIndexFailed()) :
                    dispatch(getSummaryMapdolFailed()),
            },
          ],
        }}
      />



    </>
  );
}
