import React from 'react';
import { DataGrid } from '@mui/x-data-grid';
import { useDispatch, useSelector } from 'react-redux';
import { faFileImage } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import * as evdActions from '../../../actions/evd.action';
import * as hsfsActions from '../../../actions/hsfs.action';
import './DataTable.css';
import { Box } from '@mui/material';

export default function DataTable({ type, printplate_type_seq }) {
  const imageReducer = useSelector((state) => state.imageReducer);
  const hsfsReducer = useSelector((state) => state.hsfsReducer);
  const dispatch = useDispatch();

  const columns = type == 'detailHsfs' ? [
    { field: 'id', headerName: '', width: 60, align: 'center' },
    { field: 'DTM', headerName: 'วันที่', width: 120 },
    { field: 'ORD_INX', headerName: 'ลำดับ', width: 150 },
    { field: 'REGIST', headerName: 'ประเภทจดทะเบียน', width: 150 },
    { field: 'DOC_NAME', headerName: 'ภาพลักษณ์สารบบ', width: 150 },
    { field: 'FILENAME', headerName: 'HSFS_FILENAME_P1', width: 380, align: 'center' },
    { field: 'FILENAME_2', headerName: 'HSFS_FILENAME_P2', width: 380, align: 'center' },
    {
      field: 'PATH', headerName: 'ดูภาพ', width: 60, align: 'center', renderCell: (params) => (
        <div onClick={() => dispatch(hsfsActions.displayIndexHsfs(params.value))}>
          <FontAwesomeIcon icon={faFileImage} size='2x' color='green' />
        </div>
      ),
    }
  ] : type == 'summary' ? [
    { field: 'id', headerName: '', width: 60, align: 'center' },
    { field: 'LANDOFFICE_NAME_TH', headerName: 'สำนักงานที่ดิน', width: 450 },
    { field: 'NONE_UPDATE', headerName: 'จำนวนภาพที่ไม่ปรับปรุงวันที่สแกนภาพลักษณ์', width: 300, align: 'center' },

  ] : type == 'image' ? [
    { field: 'id', headerName: '', width: 60, align: 'center', },
    { field: 'PARCEL_NO', headerName: 'เลขเอกสารสิทธิ', width: 120 },
    { field: 'AMPHUR_NAME', headerName: 'อำเภอ', width: 150 },
    { field: 'TAMBOL_NAME', headerName: 'ตำบล', width: 150 },
    { field: 'PARCEL_SURVEY_NO', headerName: 'หน้าสำรวจ', width: 150 },
    { field: 'PARCEL_INDEX_REGDTM', headerName: 'วันที่จดทะเบียน', width: 200 },
    { field: 'LAST_UPD_DTM_IMG', headerName: 'วันที่ของภาพลักษณ์', width: 200 },
    {
      field: printplate_type_seq == 0 ? 'PARCEL_SEQ' : 'PARCEL_LAND_SEQ', headerName: 'ดูภาพ', width: 60, align: 'center', renderCell: (params) => (
        <div onClick={() => dispatch(evdActions.displayParcelImage(params.value, printplate_type_seq))}>
          <FontAwesomeIcon icon={faFileImage} size='2x' color='green' />
        </div>

      ),
    },
  ] : [
    { field: 'id', headerName: '', width: 60, align: 'center' },
    { field: 'PARCEL_NO', headerName: 'เลขเอกสารสิทธิ', width: 120 },
    { field: 'AMPHUR_NAME', headerName: 'อำเภอ', width: 150 },
    { field: 'TAMBOL_NAME', headerName: 'ตำบล', width: 150 },
    { field: 'PARCEL_SURVEY_NO', headerName: 'หน้าสำรวจ', width: 150 },
    { field: 'PARCEL_INDEX_REGDTM', headerName: 'วันที่จดทะเบียน', width: 150 },
    { field: 'LAST_SCAN_DTM', headerName: 'วันที่ของภาพสารบบ', width: 150 },
    { field: 'REGISTER_NAME', headerName: 'รายการจดทะเบียน', width: 200 },
    {
      field: printplate_type_seq == 0 ? 'PARCEL_SEQ' : 'PARCEL_LAND_SEQ', headerName: 'ดูภาพ', width: 60, align: 'center', renderCell: (params) => (
        <div onClick={() => type == 'hsfs' ? dispatch(evdActions.displayParcelHsfs(params.value, printplate_type_seq)) : dispatch(hsfsActions.search(params.value))}>
          <FontAwesomeIcon icon={faFileImage} size='2x' color='green' />
        </div>

      ),
    },
  ];

  return (

    <Box style={{ height: '65vh', width: '100%', backgroundColor: 'white' }}>
      <DataGrid
        rows={type == 'image' ? imageReducer.result : hsfsReducer.result}
        columns={columns}
        pageSize={10}
        autoHeight
      />

    </Box>
  );
}
