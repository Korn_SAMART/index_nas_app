import React, { useState } from "react";
import CanvasJSReact from "../../../assets/canvasjs.react";
import { useDispatch, useSelector } from "react-redux";
import * as printplateActions from "../../../actions/printplate_top.action";


var CanvasJS = CanvasJSReact.CanvasJS;
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

CanvasJS.addColorSet("colorset", ["#28a745", "#dc3545"]);
CanvasJS.addColorSet("printplate_type_colorset", ["#3366cc", "#dc3912", "#ff9900", "#109618", "#990099", "#0099c6", "#dd4477"]);
CanvasJS.addColorSet("greenShades", [
  //colorSet Arra
  "#CEFAD0",
  "#CEFAD0",
  "#ABF7B1",
  "#83F28F",
  "#5CED73",
  "#39E75F",
  "#1FD655",
  "#00C04B",
  "#00AB41",
  "#008631",
]);

CanvasJS.addColorSet("redShades", [
  //colorSet Array
  "#FFCBD1",
  "#FFCBD1",
  "#F69697",
  "#EE6B6E",
  "#F94449",
  "#FF2C2C",
  "#F01E2C",
  "#DE0A26",
  "#D1001F",
  "#C30010",
]);

export default function Donut({ landoffice_seq, data }) {
  const dispatch = useDispatch();
  const dashboardReducer = useSelector((state) => state.dashboardReducer);
 

  return (
    <>
      <CanvasJSChart
        options={{
          colorSet: "colorset",
          animationEnabled: true,
          backgroundColor: 'transparent',
          legend: {
            fontColor: "white",
            fontSize: 20,
            fontFamily: "Kanit"
          },
          data: [
            {
              type: "doughnut",
              showInLegend: true,
              indexLabelFontColor: "white",
              indexLabelFontSize: 30,
              indexLabelFontFamily: "Kanit",
              indexLabelMaxWidth: 150,
              innerRadius: '70%',
              dataPoints: [
                {
                  y: data.success_value,
                  legendText: data.success_label,
                  indexLabel: data.success_value + "% (" + data.success_count + ")",
                  error_sts: 0,
                },
                {
                  y: data.error_value,
                  legendText: data.error_label,
                  indexLabel: data.error_value + "% (" + data.error_count + ")",
                  error_sts: 1,
                },
              ],
            },
          ],
        }}
      /> 
    </>
  );
}
