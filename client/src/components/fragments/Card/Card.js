import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react'
import './Card.css';

export default function Card({ title, value, icon }) {
  return (
    <>
      <div className="card-stats">
        <div className='row'>
          <div className='col-3 col-sm-4 col-md col-xl-3'>
            <span className='card-icon'><FontAwesomeIcon icon={icon} size='5x' /></span>
          </div>
          <div className='col col-md'>
            <div className="card-body">
              <h4 className="card-title">{title}</h4>
              <span className="card-value font-weight-bold mb-0">{value}</span>
            </div>
          </div>
        </div>

      </div>
    </>
  )
}
