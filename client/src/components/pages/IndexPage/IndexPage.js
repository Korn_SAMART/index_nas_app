import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Donut from "../../fragments/Donut";
import Barchart from "../../fragments/Barchart";
import ColumnChart from '../../fragments/ColumnChart/ColumnChart';
import StackChart from "../../fragments/StackChart/StackChart";
import * as dashboardActions from '../../../actions/dashboard.action';

export default function IndexPage() {
  const dispatch = useDispatch();

  const dashboardReducer = useSelector((state) => state.dashboardReducer);
  const [landoffice_seq, setLandoffice_seq] = useState(null);
  const [province_seq, setProvince_seq] = useState('default');
  const [region_seq, setRegion_seq] = useState('default');
  const [openModal, setOpenModal] = useState(true);


  const regionReducer = useSelector((state) => state.regionReducer);
  const provinceReducer = useSelector((state) => state.provinceReducer);

  const handleSubmit = async () => {

    setOpenModal(true);
    console.log(openModal)
    if (province_seq != 'default' || region_seq != 'default') await dispatch(dashboardActions.getDashboard({ region_seq: region_seq, province_seq: province_seq }))
    else await dispatch(dashboardActions.restoreDashboard())

    setOpenModal(false);
    console.log(openModal)
  }

  return (
    <>
      {(
        <div style={{ display: "flex", justifyContent: "center" }}>
          <div
            className="row"
            style={{
              padding: "10px",
              color: "white",
              background: "rgb(0, 41, 63)",
              width: "100%",
              borderRadius: "5px",
            }}
          >
            <div className="col-3">
              <label for="" style={{ fontWeight: 600 }}>
                ภูมิภาค
              </label>
              <select
                defaultValue="default"
                onChange={(e) => setRegion_seq(e.target.value)}
                className="form-select"
                aria-label=".form-select-lg example"
                id="region_seq"
                name="region_seq"
              >
                <option value="default" key="default" selected>-- ทุกภูมิภาค --</option>
                {regionReducer.result &&
                  regionReducer.result.map((region) => {
                    return (
                      <option
                        key={region.REGION_SEQ}
                        value={region.REGION_SEQ}
                      >
                        {region.REGION_NAME}
                      </option>
                    );
                  })}
              </select>
            </div>
            <div className="col-6">
              <label for="" style={{ fontWeight: 600 }}>
                จังหวัด
              </label>
              <select
                defaultValue='default'
                onChange={(e) => setProvince_seq(e.target.value)}
                className="form-select"
                aria-label=".form-select-lg example"
                id="province_seq"
                name="province_seq"
              >
                <option value='default' selected>-- ทุกจังหวัด --</option>
                {provinceReducer.result &&
                  provinceReducer.result.map((prov) => {
                    return (
                      <option key={prov.PROVINCE_SEQ} value={prov.PROVINCE_SEQ}>
                        {prov.PROVINCE_NAME}
                      </option>
                    );
                  })}
              </select>
            </div>
            <div className="col-3">
              <button onClick={handleSubmit} style={{ width: '100%', height: '100%' }} className="btn btn-primary btn-block">ค้นหา</button>
            </div>
          </div>
        </div>
      )}
      <div className="donut-card">
        <h2>กราฟแสดงผลการเชื่อมโยงทะเบียนและสารบบ</h2>
        <div className="row inner-donut-card">
          <div className="col-7 total-donut-chart">
            <Donut
              landoffice_seq={landoffice_seq}
              data={{
                success_label: "สารบบที่เชื่อมโยงสำเร็จ",
                success_value: (
                  ((dashboardReducer.image.filter((i) => i.STS_UPD == null).length + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null).length) /
                    (dashboardReducer.image.length + dashboardReducer.image.length)) *
                  100
                ).toFixed(2),
                success_count: (dashboardReducer.image.filter((i) => i.STS_UPD == null).length + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null).length).toLocaleString(),
                error_label: "สารบบที่เชื่อมโยงไม่สำเร็จ",
                error_value: (
                  ((dashboardReducer.image.filter((i) => i.STS_UPD != null).length + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null).length) /
                    (dashboardReducer.image.length + dashboardReducer.image.length)) *
                  100
                ).toFixed(2),
                error_count: (dashboardReducer.image.filter((i) => i.STS_UPD != null).length + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null).length).toLocaleString(),
                datatype: "image",
              }}
            />
            <div className="row">
              <div className="col">
                <StackChart datatype='summary_image' />
              </div>
            </div>
          </div>

          <div className="col">
            <div>
              <ColumnChart title="ประเภทเอกสารสิทธิที่ไม่สามารถเชื่อมสารบบได้" datatype='detail_index_unmatched' />
              <ColumnChart title="ประเภทเอกสารสิทธิที่ไม่ปรับปรุงวันที่สแกนสารบบ" datatype='detail_index_noneupdate' />
            </div>

          </div>
        </div>

        <div className="row">
          <div className="col">
            <Barchart
              title="10 ลำดับสำนักงานที่ดินที่ Matched สารบบมากที่สุด"
              datatype="image"
              top10={dashboardReducer.top10_success.index}
              isSuccess
            />
          </div>
          <div className="col">
            <Barchart
              title="10 ลำดับสำนักงานที่ดินที่ Unmatched สารบบมากที่สุด"
              datatype="image"
              top10={dashboardReducer.top10_error.index}
            />
          </div>
        </div>
      </div>
    </>
  )
}
