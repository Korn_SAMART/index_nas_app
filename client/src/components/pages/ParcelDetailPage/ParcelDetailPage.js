import React, { useState } from "react";
import DataTable from "../../fragments/DataTable/DataTable";
import "bootstrap/dist/css/bootstrap.min.css";
import { useDispatch, useSelector } from "react-redux";
import { Pagination } from "@mui/material";
import Modal from "../../fragments/Modal";
import { makeStyles } from "@mui/styles";

const useStyles = makeStyles(() => ({
  ul: {
    "& .MuiPaginationItem-root": {
      color: "#fff"
    },
    "&": {
      justifyContent: 'space-between'
    }
  }
}));

export default function ParcelDetailPage({ type, printplate_type_seq }) {
  const evdReducer = useSelector((state) => state.evdReducer);
  const [pagesNo, setPagesNo] = useState(1);
  const [openModal, setOpenModal] = useState(false);
  const classes = useStyles();

  const handleChangePage = (_, value) => {
    if (value !== null) {
      setPagesNo(value);
      if (
        evdReducer.result[value - 1].PARCEL_IMAGE_URL &&
        evdReducer.result[value - 1].PARCEL_IMAGE_FILENAME
      ) {
        setOpenModal(true);
      }
    }
  };

  return (
    <>
      <div className="row" style={{ marginTop: '30px' }}>
        <div className="col">
          <div style={{ padding: '30px', backgroundColor: 'rgb(0, 41, 63)' }}>
            <DataTable type={type} printplate_type_seq={printplate_type_seq} />
          </div>

        </div>

        {!evdReducer.isFetching &&
          !evdReducer.result &&
          !evdReducer.isFetching &&
          evdReducer.isSearch &&
          !!evdReducer.result &&
          evdReducer.result.length <= 0 && (
            <div className="col-4">
              <div
                style={{
                  padding: "5px",
                  backgroundColor: "rgb(143, 195, 122)",
                }}
              >
                <h5>ภาพลักษณ์</h5>
              </div>
              <h4 style={{ textAlign: "center" }}>ไม่พบรูปภาพแปลง</h4>
            </div>
          )}

        {!evdReducer.isFetching && evdReducer.result && evdReducer.result.length > 0 && (
          <div className="col-4">
            <Pagination
              classes={{ ul: classes.ul }}
              page={pagesNo}
              onChange={handleChangePage}
              count={evdReducer.result.length}
              color="primary"
            />
            <h5 style={{ textAlign: "center", color: 'white' }}>
              {evdReducer.result[pagesNo - 1].PARCEL_IMAGE_PNAME}
            </h5>
            <img
              onLoad={() => setOpenModal(false)}
              src={
                "http://ilands.dol.go.th/evdservice/servlet/EvdImageUnCompressServlet?plateType=1&readOnly=2&indexSeq=&action=evd&systemType=1&landofficeSeq=" +
                localStorage.getItem("landoffice_seq") +
                "&image=" +
                evdReducer.result[pagesNo - 1].PARCEL_IMAGE_URL +
                "/" +
                evdReducer.result[pagesNo - 1].PARCEL_IMAGE_FILENAME
              }
              width="100%"
              alt="evd"
              id="image_evd"
            />
          </div>
        )}
      </div>

      <Modal open={openModal} />
    </>
  );
}
