import React, { useEffect, useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import { useDispatch, useSelector } from "react-redux";
import * as dashboardActions from "../../../actions/dashboard.action";
import Donut from "../../fragments/Donut";
import Card from "../../fragments/Card";
import "./SummaryPage.css";
import { faFolderOpen, faImage, faFileText, faMap } from "@fortawesome/free-solid-svg-icons";
import Barchart from "../../fragments/Barchart";
import StackChart from "../../fragments/StackChart/StackChart";
import Modal from "../../fragments/Modal";
import { getProvinces, getRegion } from "../../../actions/master.action";

export default function SummaryPage() {
  const dispatch = useDispatch();
  const dashboardReducer = useSelector((state) => state.dashboardReducer);
  const printplate_topReducer = useSelector(
    (state) => state.printplate_topReducer
  );
  const regionReducer = useSelector((state) => state.regionReducer);
  const provinceReducer = useSelector((state) => state.provinceReducer);

  const [landoffice_seq, setLandoffice_seq] = useState(null);
  const [valueType, setValueType] = useState(1);
  const [province_seq, setProvince_seq] = useState('default');
  const [region_seq, setRegion_seq] = useState('default');

  useEffect(async () => {
    if (!dashboardReducer.threeway) {
      await dispatch(getProvinces());
      await dispatch(getRegion());
      await dispatch(dashboardActions.initialDashboard());

    }

  }, []);



  const handleSubmit = async () => {

    if (province_seq != 'default' || region_seq != 'default') await dispatch(dashboardActions.getDashboard({ region_seq: region_seq, province_seq: province_seq }))
    else await dispatch(dashboardActions.restoreDashboard())

  }


  return (
    <>
      {(
        <div style={{ display: "flex", justifyContent: "center" }}>
          <div
            className="row"
            style={{
              padding: "10px",
              color: "white",
              background: "rgb(0, 41, 63)",
              width: "100%",
              borderRadius: "5px",
            }}
          >
            <div className="col-3">
              <label for="" style={{ fontWeight: 600 }}>
                ภูมิภาค
              </label>
              <select
                defaultValue="default"
                onChange={(e) => setRegion_seq(e.target.value)}
                className="form-select"
                aria-label=".form-select-lg example"
                id="region_seq"
                name="region_seq"
              >
                <option value="default" key="default" selected>-- ทุกภูมิภาค --</option>
                {regionReducer.result &&
                  regionReducer.result.map((region) => {
                    return (
                      <option
                        key={region.REGION_SEQ}
                        value={region.REGION_SEQ}
                      >
                        {region.REGION_NAME}
                      </option>
                    );
                  })}
              </select>
            </div>
            <div className="col-6">
              <label for="" style={{ fontWeight: 600 }}>
                จังหวัด
              </label>
              <select
                defaultValue='default'
                onChange={(e) => setProvince_seq(e.target.value)}
                className="form-select"
                aria-label=".form-select-lg example"
                id="province_seq"
                name="province_seq"
              >
                <option value='default' selected>-- ทุกจังหวัด --</option>
                {provinceReducer.result &&
                  provinceReducer.result.map((prov) => {
                    return (
                      <option key={prov.PROVINCE_SEQ} value={prov.PROVINCE_SEQ}>
                        {prov.PROVINCE_NAME}
                      </option>
                    );
                  })}
              </select>
            </div>
            <div className="col-3">
              <button onClick={handleSubmit} style={{ width: '100%', height: '100%' }} className="btn btn-primary btn-block">ค้นหา</button>
            </div>
          </div>
        </div>
      )}

      {dashboardReducer.isFetching || printplate_topReducer.isFetching ? (
        <Modal open={dashboardReducer.isFetching} />
      ) : (
        dashboardReducer.image &&
        dashboardReducer.mapdol &&

        (
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              rowGap: "20px",
              marginTop: "20px",
            }}
          >
            <div className="row summary-card">
              <div className="col">
                <Card
                  title="ข้อมูลทะเบียน"
                  value={Number(dashboardReducer.summary.reg).toLocaleString()}
                  icon={faFileText}

                />
              </div>
              <div className="col">
                <Card
                  title="ข้อมูลภาพลักษณ์"
                  value={Number(dashboardReducer.summary.image).toLocaleString()}
                  icon={faImage}
                />
              </div>
              {/* <div className="col">
                <Card
                  title="ข้อมูลสารบบ"
                  value={Number(dashboardReducer.index_matched.length).toLocaleString()}
                  icon={faFolderOpen}
                />
              </div> */}
              <div className="col">
                <Card
                  title="ข้อมูลรูปแปลง"
                  value={Number(dashboardReducer.summary.mapdol).toLocaleString()}
                  icon={faMap}
                />
              </div>
            </div>
            {/* 3 ส่วน */}
            <div className="donut-card">
              <h2>กราฟแสดงผลการเชื่อมโยง 3 ส่วน (ทะเบียน + ภาพลักษณ์ + รูปแปลง)</h2>
              <div className="row inner-donut-card">
                <div className="col-7 total-donut-chart">
                  <Donut
                    landoffice_seq={landoffice_seq}
                    data={{
                      success_label: "เอกสารสิทธิที่เชื่อมโยงได้",
                      success_value: (
                        (dashboardReducer.threeway.filter((i) => i.UTMMAP1 != null || i.UTMMAP3 != null).length / dashboardReducer.threeway.length) * 100
                      ).toFixed(2),
                      success_count: dashboardReducer.threeway.filter((i) => i.UTMMAP1 != null || i.UTMMAP3 != null).length.toLocaleString(),
                      error_label: "เอกสารสิทธิที่เชื่อมโยงไม่ได้",
                      error_value: (
                        (dashboardReducer.threeway.filter((i) => i.UTMMAP1 == null || i.UTMMAP3 == null).length / dashboardReducer.threeway.length) * 100
                      ).toFixed(2),
                      error_count: dashboardReducer.threeway.filter((i) => i.UTMMAP1 == null || i.UTMMAP3 == null).length.toLocaleString(),
                      datatype: "threeway",
                    }}
                  />
                  {valueType == 1 && <div className="row">
                    <div className="col">
                      <StackChart datatype='summary_three' />
                    </div>
                  </div>}

                </div>



                {valueType == 1 && <div className="col">

                  <div>
                    <Barchart
                      title="10 ลำดับสำนักงานที่ดินที่เชื่อมโยง 3 ส่วนได้มากที่สุด"
                      datatype="threeway"
                      top10={dashboardReducer.top10_success.threeway}
                      isSuccess
                    />
                    <Barchart
                      title="10 ลำดับสำนักงานที่ดินที่เชื่อมโยง 3 ส่วนไม่ได้มากที่สุด"
                      datatype="threeway"
                      top10={dashboardReducer.top10_error.threeway}
                    />
                  </div>
                </div>}
              </div>
            </div>

            {/* ภาพลักษณ์ */}
            <div className="donut-card">
              <h2>กราฟแสดงผลการเชื่อมโยงทะเบียนและภาพลักษณ์เอกสารสิทธิ</h2>
              <div className="row inner-donut-card">
                <div className="col-7 total-donut-chart">
                  <Donut
                    landoffice_seq={landoffice_seq}
                    data={{
                      success_label: "ภาพลักษณ์ที่เชื่อมโยงได้",
                      success_value: (
                        ((dashboardReducer.image.filter((i) => i.STS_UPD == null).length + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null).length) /
                          (dashboardReducer.image.length + dashboardReducer.image.length)) *
                        100
                      ).toFixed(2),
                      success_count: (dashboardReducer.image.filter((i) => i.STS_UPD == null).length + dashboardReducer.image.filter((i) => i.PARCEL_SEQ != null).length).toLocaleString(),
                      error_label: "ภาพลักษณ์ที่เชื่อมโยงไม่ได้",
                      error_value: (
                        ((dashboardReducer.image.filter((i) => i.STS_UPD != null).length + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null).length) /
                          (dashboardReducer.image.length + dashboardReducer.image.length)) *
                        100
                      ).toFixed(2),
                      error_count: (dashboardReducer.image.filter((i) => i.STS_UPD != null).length + dashboardReducer.image.filter((i) => i.PARCEL_SEQ == null).length).toLocaleString(),
                      datatype: "image",
                    }}
                  />
                  {valueType == 1 && <div className="row">
                    <div className="col">
                      <StackChart datatype='summary_image' />
                    </div>
                  </div>}

                </div>

                {valueType == 2 && <div className="row">
                  <div className="col">
                    <StackChart datatype='summary_image' />
                  </div>
                </div>}

                {valueType == 1 && <div className="col">

                  <div>
                    <Barchart
                      title="10 ลำดับสำนักงานที่ดินที่เชื่อมโยงภาพลักษณ์เอกสารสิทธิได้มากที่สุด"
                      datatype="image"
                      top10={dashboardReducer.top10_success.image}
                      isSuccess
                    />
                    <Barchart
                      title="10 ลำดับสำนักงานที่ดินที่เชื่อมโยงภาพลักษณ์เอกสารสิทธิไม่ได้มากที่สุด"
                      datatype="image"
                      top10={dashboardReducer.top10_error.image}
                    />
                  </div>
                </div>}
              </div>
            </div>

            {/* สารบบ */}
            {/* <div className="donut-card">
              <h2>กราฟแสดงผลการเชื่อมโยงทะเบียนและสารบบ</h2>
              <div className="row inner-donut-card">
                <div className="col-7 total-donut-chart">
                  <Donut
                    landoffice_seq={landoffice_seq}
                    data={{
                      success_label: "สารบบที่ปรับปรุงวันที่สแกนแล้ว",
                      success_value: (
                        (dashboardReducer.index_noneupdate.filter((i) => i.STS_UPD == null)
                          .length /
                          dashboardReducer.index_noneupdate.length) *
                        100
                      ).toFixed(2),
                      success_count: dashboardReducer.index_noneupdate.filter((i) => i.STS_UPD == null).length.toLocaleString(),
                      error_label: "สารบบที่ยังไม่ปรับปรุงวันที่สแกน",
                      error_value: (
                        (dashboardReducer.index_noneupdate.filter((i) => i.STS_UPD != null)
                          .length /
                          dashboardReducer.index_noneupdate.length) *
                        100
                      ).toFixed(2),
                      error_count: dashboardReducer.index_noneupdate.filter((i) => i.STS_UPD != null).length.toLocaleString(),
                      datatype: "index_noneupdate",
                    }}
                  />
                  {valueType == 1 && <div className="row">
                    <div className="col">
                      <StackChart datatype='summary_index' data={dashboardReducer.index_noneupdate} />

                    </div>
                  </div>}

                </div>

                {valueType == 2 && <div className="row">
                  <div className="col">
                    <StackChart  datatype='summary_index' data={dashboardReducer.index_noneupdate} />

                  </div>
                </div>}

                {valueType == 1 && <div className="col">

                  <div>
                    <Barchart
                      title="10 ลำดับสำนักงานที่ดินที่ปรับปรุงวันที่สแกนสารบบเอกสารสิทธิมากที่สุด"
                      datatype="index"
                      top10={dashboardReducer.top10_success.index}
                      isSuccess
                    />
                    <Barchart
                      title="10 ลำดับสำนักงานที่ดินที่ไม่ปรับปรุงวันที่สแกนสารบบเอกสารสิทธิมากที่สุด"
                      datatype="index"
                      top10={dashboardReducer.top10_error.index}
                    />
                  </div>

                </div>}
              </div>
            </div> */}

            <div className="donut-card">
              <h2>กราฟแสดงผลการเชื่อมโยงทะเบียนและรูปแปลง</h2>
              <div className="row inner-donut-card">
                <div className="col total-donut-chart">
                  <Donut
                    landoffice_seq={landoffice_seq}
                    data={{
                      success_label: "รูปแปลงที่เชื่อมโยงได้",
                      success_value: (
                        (dashboardReducer.mapdol.filter((i) => i.UTMMAP1 != null || i.UTMMAP3 != null)
                          .length /
                          dashboardReducer.mapdol.length) *
                        100
                      ).toFixed(2),
                      success_count: dashboardReducer.mapdol.filter((i) => i.UTMMAP1 != null || i.UTMMAP3 != null).length.toLocaleString(),
                      error_label: "รูปแปลงที่เชื่อมโยงไม่ได้",
                      error_value: (
                        (dashboardReducer.mapdol.filter((i) => i.UTMMAP1 == null || i.UTMMAP3 == null)
                          .length /
                          dashboardReducer.mapdol.length) *
                        100
                      ).toFixed(2),
                      error_count: dashboardReducer.mapdol.filter((i) => i.UTMMAP1 == null || i.UTMMAP3 == null).length.toLocaleString(),
                      datatype: "mapdol",
                    }}
                  />
                  {valueType == 1 && <div className="row">
                    <div className="col">
                      <StackChart datatype='summary_mapdol' />
                    </div>
                  </div>}

                </div>
                {valueType == 2 && <div className="row">
                  <div className="col">
                    <StackChart datatype='summary_mapdol' />
                  </div>
                </div>}

                {valueType == 1 && <div className="col">
                  <div>
                    <Barchart
                      title="10 ลำดับสำนักงานที่ดินที่เชื่อมโยงรูปแปลงได้มากที่สุด"
                      datatype="image"
                      top10={dashboardReducer.top10_success.mapdol}
                      isSuccess
                    />
                    <Barchart
                      title="10 ลำดับสำนักงานที่ดินที่เชื่อมโยงรูปแปลงไม่ได้มากที่สุด"
                      datatype="image"
                      top10={dashboardReducer.top10_error.mapdol}
                    />
                  </div>
                </div>}
              </div>

            </div>
          </div>
        )
      )}




    </>
  );
}
