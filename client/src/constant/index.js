export const IMAGE_FETCHING = "IMAGE_FETCHING";
export const IMAGE_SUCCESS = "IMAGE_SUCCESS";
export const IMAGE_FAILED = "IMAGE_FAILED";
export const IMAGE_CLEARED = "IMAGE_CLEARED";

export const EVD_FETCHING = "EVD_FETCHING";
export const EVD_SUCCESS = "EVD_SUCCESS";
export const EVD_FAILED = "EVD_FAILED";

export const INDEX_FETCHING = "INDEX_FETCHING";
export const INDEX_SUCCESS = "INDEX_SUCCESS";
export const INDEX_FAILED = "INDEX_FAILED";

export const SUMMARY_FETCHING = "SUMMARY_FETCHING";
export const SUMMARY_SUCCESS = "SUMMARY_SUCCESS";
export const SUMMARY_FAILED = "SUMMARY_FAILED";

export const DASHBOARD_FETCHING = "DASHBOARD_FETCHING";
export const DASHBOARD_SUCCESS = "DASHBOARD_SUCCESS";
export const DASHBOARD_FILTER_SUCCESS = 'DASHBOARD_FILTER_SUCCESS';
export const DASHBOARD_FAILED = "DASHBOARD_FAILED";
export const DASHBOARD_CLEARED = "DASHBOARD_CLEARED";

export const PRINTPLATE_TOP_FETCHING = "PRINTPLATE_TOP_FETCHING";
export const PRINTPLATE_TOP_SUCCESS = "PRINTPLATE_TOP_SUCCESS";
export const PRINTPLATE_TOP_FAILED = "PRINTPLATE_TOP_FAILED";
export const PRINTPLATE_TOP_CLEARED = "PRINTPLATE_TOP_CLEARED";

export const PROVINCE_FETCHING = 'PROVINCE_FETCHING';
export const PROVINCE_SUCCESS = 'PROVINCE_SUCCESS';
export const PROVINCE_FAILED = 'PROVINCE_FAILED';

export const REGION_FETCHING = 'REGION_FETCHING';
export const REGION_SUCCESS = 'REGION_SUCCESS';
export const REGION_FAILED = 'REGION_FAILED';
