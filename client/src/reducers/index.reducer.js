import { INDEX_FAILED, INDEX_FETCHING, INDEX_SUCCESS } from '../constant';

const initialState = {
  isError: false,
  isFetching: false,
  result: null,

}


export default (state = initialState, { type, payload }) => {
  switch (type) {

    case INDEX_FETCHING:
      return { ...state, isFetching: true, isError: false }
    case INDEX_SUCCESS:
      return { ...state, isFetching: false, isError: false, result: payload }
    case INDEX_FAILED:
      return { ...state, isFetching: false, isError: true }
    default:
      return state
  }
}

