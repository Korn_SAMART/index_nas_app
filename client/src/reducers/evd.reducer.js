import { EVD_FETCHING, EVD_SUCCESS, EVD_FAILED } from '../constant';

const initialState = {
  isError: false,
  isFetching: false,
  isSearch: false,
  result: null,
}


export default (state = initialState, { type, payload }) => {
  switch (type) {
    case EVD_FETCHING:
      return { ...state, isFetching: true, isError: false, isSearch: true, }
    case EVD_SUCCESS:
      return { ...state, isFetching: false, isError: false, isSearch: true, result: payload }
    case EVD_FAILED:
      return { ...state, isFetching: false, isError: true, isSearch: true, }

    default:
      return state
  }
}

