import { combineReducers } from 'redux';
import dashboardReducer from './dashboard.reducer';
import evdReducer from './evd.reducer';
import imageReducer from './image.reducer';
import indexReducer from './index.reducer';
import printplate_topReducer from './printplate_top.reducer';
import provinceReducer from './province.reducer';
import regionReducer from './region.reducer';
import summaryReducer from './summary.reducer';

export default combineReducers({
  imageReducer, evdReducer, indexReducer, summaryReducer, dashboardReducer, printplate_topReducer, provinceReducer, regionReducer

})

