import { PROVINCE_FAILED, PROVINCE_FETCHING, PROVINCE_SUCCESS } from "../constant"


const initialState = {
  isError: false,
  isFetching: false,
  result: null,
}


export default (state = initialState, { type, payload }) => {
  switch (type) {

    case PROVINCE_FETCHING:
      return { ...state, isFetching: true, isError: false }
    case PROVINCE_SUCCESS:
      return { ...state, isFetching: false, isError: false, result: payload }
    case PROVINCE_FAILED:
      return { ...state, isFetching: false, isError: true }
    default:
      return state
  }
}

