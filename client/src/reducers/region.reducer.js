import { REGION_FAILED, REGION_FETCHING, REGION_SUCCESS } from "../constant"


const initialState = {
  isError: false,
  isFetching: false,
  result: null,
}


export default (state = initialState, { type, payload }) => {
  switch (type) {

    case REGION_FETCHING:
      return { ...state, isFetching: true, isError: false }
    case REGION_SUCCESS:
      return { ...state, isFetching: false, isError: false, result: payload }
    case REGION_FAILED:
      return { ...state, isFetching: false, isError: true }
    default:
      return state
  }
}

