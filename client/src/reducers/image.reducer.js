import { IMAGE_FETCHING, IMAGE_SUCCESS, IMAGE_FAILED, IMAGE_CLEARED } from '../constant';

const initialState = {
  isError: false,
  isFetching: false,
  result: null,
  printplate_type_seq: 0
}


export default (state = initialState, { type, payload, params }) => {
  switch (type) {

    case IMAGE_FETCHING:
      return { ...state, isFetching: true, isError: false }
    case IMAGE_SUCCESS:
      if (params) return { ...state, isFetching: false, isError: false, result: payload, printplate_type_seq: params }
      return { ...state, isFetching: false, isError: false, result: payload }
    case IMAGE_FAILED:
      return { ...state, isFetching: false, isError: true }
    case IMAGE_CLEARED:
      return initialState;
    default:
      return state
  }
}

