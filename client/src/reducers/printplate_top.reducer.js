import { PRINTPLATE_TOP_CLEARED, PRINTPLATE_TOP_FAILED, PRINTPLATE_TOP_FETCHING, PRINTPLATE_TOP_SUCCESS } from "../constant";

const initialState = {
  isError: false,
  isFetching: false,
  result: null,
  datatype: 'image'
}

export default (state = initialState, { type, payload, datatype }) => {
  switch (type) {

    case PRINTPLATE_TOP_FETCHING:
      return { ...state, isFetching: true, isError: false }
    case PRINTPLATE_TOP_SUCCESS:
      return { ...state, isFetching: false, isError: false, result: payload, datatype: datatype }
    case PRINTPLATE_TOP_FAILED:
      return { ...state, isFetching: false, isError: true }
    case PRINTPLATE_TOP_CLEARED:
      return initialState;
    default:
      return state;
  }
}

