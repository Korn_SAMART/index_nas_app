import {
  DASHBOARD_CLEARED,
  DASHBOARD_FAILED,
  DASHBOARD_FETCHING,
  DASHBOARD_FILTER_SUCCESS,
  DASHBOARD_SUCCESS,
} from "../constant";

const initialState = {
  isError: false,
  isFetching: true,
  image: null,
  index: null,
  mapdol: null,
  threeway: null,
  top10_error: {
    image: null,
    index: null,
    mapdol: null,
    threeway: null,
  },
  top10_success: {
    image: null,
    index: null,
    mapdol: null,
    threeway: null,
  },
  top10_noneupdate: {
    image: null
  },
  overall_image: null,
  overall_index: null,
  overall_mapdol: null,
  overall_threeway: null,
  summary: {
    reg: 0,
    image: 0,
    index: 0,
    mapdol: 0
  }
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case DASHBOARD_FETCHING:
      return { ...state, isFetching: true, isError: false };
    case DASHBOARD_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        image: payload.image,
        index: payload.index,
        mapdol: payload.mapdol,
        threeway: payload.threeway,
        top10_error: {
          image: payload.top10_error.image,
          index: payload.top10_error.index,
          mapdol: payload.top10_error.mapdol,
          threeway: payload.top10_error.threeway
        },
        top10_success: {
          image: payload.top10_success.image,
          index: payload.top10_success.index,
          mapdol: payload.top10_success.mapdol,
          threeway: payload.top10_success.threeway
        },
        top10_noneupdate: {
          image: payload.top10_noneupdate.image
        },
        overall_image: payload.image,
        overall_index: payload.index,
        overall_mapdol: payload.mapdol,
        overall_threeway: payload.threeway,
        summary: {
          reg: payload.reg_count,
          image: payload.image_count,
          index: payload.index_count,
          mapdol: payload.mapdol_count
        }
      };
    case DASHBOARD_FAILED:
      return { ...state, isFetching: false, isError: true };
    case DASHBOARD_FILTER_SUCCESS:
      return {
        ...state,
        isFetching: false,
        isError: false,
        image: payload.image,
        index: payload.index,
        mapdol: payload.mapdol,
        threeway: payload.threeway,
      };
    case DASHBOARD_CLEARED:
      return {
        ...state,
        isFetching: false,
        isError: false,
        image: payload.image,
        index: payload.index,
        mapdol: payload.mapdol,
        threeway: payload.threeway,
      };

    default:
      return state;
  }
};
