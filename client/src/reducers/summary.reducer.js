import { SUMMARY_FAILED, SUMMARY_FETCHING, SUMMARY_SUCCESS } from "../constant"

const initialState = {
  isError: false,
  isFetching: false,
  result: null,
  hsfs: null

}

export default (state = initialState, { type, payload }) => {
  switch (type) {

    case SUMMARY_FETCHING:
      return { ...state, isFetching: true, isError: false }
    case SUMMARY_SUCCESS:
      payload.map(data => {
        data.IMAGE_NONE_UPDATE = Number(data.IMAGE_NONE_UPDATE).toLocaleString();
        data.HSFS_NONE_UPDATE = Number(data.HSFS_NONE_UPDATE).toLocaleString();
      });
      return { ...state, isFetching: false, isError: false, result: payload }
    case SUMMARY_FAILED:
      return { ...state, isFetching: false, isError: true }
    default:
      return state;
  }
}

